using Sirenix.OdinInspector;
using UnityEngine;


public class ShopItem : Socializeable
{
#region Fields
    [Required] public Item_SOS _item;
    private ShopKeeper _sk;
#endregion


#region Private Methods
    private void Awake()
    {
        _sk = gameObject.GetComponentInParent<ShopKeeper>();
        InteractionStart.AddListener(BuyThis);
        InteractionStart.AddListener(gameObject => _sk.Buy(gameObject));
    }
    private void BuyThis(GameObject player)
    {
        _sk.SetBuyingObject(_item);
    }
#endregion
}