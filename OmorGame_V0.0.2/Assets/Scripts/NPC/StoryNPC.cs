using System;
using System.Collections;
using UnityEngine;


[Serializable]
public class StoryNPC : Socializeable
{
#region Public Methods
    public override void StartInteraction(GameObject gameObject)
    {
        InteractionStart.Invoke(gameObject);
        StartCoroutine(wait(gameObject));
    }
    public IEnumerator wait(GameObject player)
    {
        yield return new WaitForSeconds(10f);
        InteractionEnd.Invoke(player);
        StartInteraction(player);
    }
#endregion


#region Private Methods
    private void OnTriggerEnter2D(Collider2D other)
    {
        StartInteraction(other.gameObject);
    }
#endregion
}