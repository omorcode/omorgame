using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;


public class Socializeable : SerializedMonoBehaviour
{
#region Fields
    public List<string> DialogueList = new List<string>();
    public UnityEvent<GameObject> InteractionEnd;
    public UnityEvent<GameObject> InteractionStart;
    public GameObject pfbSpeechBubble;
    public int startDialogIndex;
    private int _textIndex;
    private GameObject _clone;
#endregion


#region Public Methods
    public void SaySomething(int index)
    {
        if (_clone == null) _clone = Instantiate(pfbSpeechBubble, transform);
        _clone.GetComponentInChildren<FancySpeechBubble>().Set(DialogueList[index]);
    }
    public void SaySomething()
    {
        SaySomething(_textIndex);
        _textIndex = Mathf.Min(++_textIndex, DialogueList.Count - 1);
    }
    [Button]
    public virtual void StartInteraction(GameObject gameObject)
    {
        InteractionStart?.Invoke(gameObject);
    }
    public void StopTalking()
    {
        if (_clone == null) return;
        Destroy(_clone.gameObject);
        _clone = null;
    }
#endregion


#region Private Methods
    private void Awake()
    {
        _textIndex = startDialogIndex;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        StartInteraction(other.gameObject);
    }
#endregion
}
public enum InteractionKind
{
    colliderTrigger,
    pressInteract
}
[Serializable] public class ObjectObjectDictionary : UnitySerializedDictionary<GameObject, GameObject> { }