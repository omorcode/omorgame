using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Dalek : Enemy
{
    public UnityEvent statusIdleEvent;
    public UnityEvent statusMovingEvent;
    public UnityEvent statusAttackingEvent;
    public DalekStatus currentStatus;
    public DalekStatus startingStatus;
    public DamageOnTouch DmgOnTouch;
    private void Start()
    {
        DmgOnTouch = GetComponentInChildren<DamageOnTouch>();
        DmgOnTouch.enemy = this;
        ChangeStatus(startingStatus);
    }
    public void ChangeStatus(DalekStatus newStatus)
    {
        switch (newStatus)
        {
            case DalekStatus.Idle:
                statusIdleEvent?.Invoke();
                break;
            case DalekStatus.Moving:
                statusMovingEvent?.Invoke();
                break;
            case DalekStatus.Attacking:
                statusAttackingEvent?.Invoke();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newStatus), newStatus, null);
        }

        currentStatus = newStatus;
    }
}

public enum DalekStatus
{
    Idle,
    Moving,
    Attacking,

}