using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = System.Random;


public class Bowman_Movement : EnemyMovementController
{
#region Fields
    [Range(1, 10)] public float bouncRange;
    public GameObject GO_Player;
    public float panickspeed;
    public float playerToCloseDistance;
    public float sattisfactionrange;
    public float searchTime;
    private Bowman _bowman;
    private Vector3 _targetPoint;
    private GameObject _targetObject;
    [ShowInInspector] private GameObject _player;
    private Vector2 _direction;
    private Rigidbody2D this_rigidbody2D;
    private int a = 1;
    private readonly Random r = new Random(62);
    private int _sattisfactionAttempts;
    private bool _isTryingToSatisfy;
    private Vector3 _playerPositionFixed;
    private float _helptimer = 4f;
#endregion


#region Public Methods
    public void FollowFellowEnemy()
    {
        var FellowEnemies = new List<GameObject>(GameManager.Main.Enemies_GameObjects.Where(g => g != gameObject));
        FellowEnemies = FellowEnemies
            .OrderByDescending(f => Vector3.Distance(f.transform.position, _player.transform.position))
            .ThenByDescending(f => Vector3.Distance(transform.position, f.transform.position))
            .ToList();

        /*TODO :
         * - it only gets the first player 
         * - for brawl room this needs to be fixed;
         * - 
         */
        _targetObject = FellowEnemies.First();
        GetTargetPoint();
        _isTryingToSatisfy = false;
        _isMoving = true;
    }
    public void GetTargetPoint()
    {
        _playerPositionFixed = _player.transform.position;
        _targetPoint = new Vector3(
            _targetObject.transform.position.x + _playerPositionFixed.x * 0.1f,
            _targetObject.transform.position.y + _playerPositionFixed.y * 0.1f,
            0);
    }
    public void PanickMovent()
    {
        movingSpeed = panickspeed;
        _isMoving = true;
        _direction = new Vector3(r.Next(), r.Next(), 0).normalized;
    }
    public void Search()
    {
        StopMoving();
        StartCoroutine(SearchRoutine());
    }
    public void StopMoving()
    {
        _isMoving = false;
        this_rigidbody2D.velocity = Vector2.zero;
    }
#endregion


#region Private Methods
    private void Awake()
    {
        _isMoving = false;
        _bowman = GetComponent<Bowman>();
        this_rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
        _sattisfactionAttempts = 5;
        playerToCloseDistance = 10;
        _player = GameManager.Main.GetRegisteredPlayers().First();
        searchTime = 1;
        _isTryingToSatisfy = false;
        panickspeed = 10;
    }
    private void FixedUpdate()
    {
        _bowman.animator.SetFloat("move x",_direction.x);
        _bowman.animator.SetFloat("move y",_direction.y);
        _bowman.animator.SetFloat("moving speed", movingSpeed);
        if (!_isMoving) return;
        this_rigidbody2D.MovePosition(this_rigidbody2D.position + _direction * movingSpeed * Time.fixedDeltaTime);

        if (_bowman.currentStatus == Bowmanstatus.Panick) return;
        _direction = (new Vector2(_targetPoint.x, _targetPoint.y) - this_rigidbody2D.position).normalized;
        _helptimer -= Time.fixedDeltaTime;
        if (Vector3.Distance(transform.position, _targetPoint) < sattisfactionrange && !_isTryingToSatisfy ||
            _helptimer <= 0)
        {
            _helptimer = 4;
            _bowman.ChangeStatus(Bowmanstatus.Shooting);
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (_bowman != null && _bowman.currentStatus == Bowmanstatus.Panick)
        {
            _isMoving = false;
            var inNormal = other.contacts[0].normal +
                           new Vector2(r.Next(1, 10) / 10 * bouncRange, r.Next(1, 10) / 10 * bouncRange).normalized;

            _direction = Vector3.Reflect(_direction, inNormal).normalized;
            _isMoving = true;
        }
    }
    /*TODO :
     * - dont pick the same seed 
     *
     */
    private IEnumerator PanicMovementCoroutine()
    {
        yield return new WaitWhile(() => _bowman.AIpaused);
        _isMoving = true;
        while (true) // endlosschleife vorsicht 
        {
            _targetPoint = new Vector3(r.Next(), r.Next(), 0);

            yield return new WaitWhile(() => _bowman.AIpaused);
            yield return new WaitForSeconds((float) r.Next(50, 250) / 100);
        }
    }
    private IEnumerator SatisfyCoroutine()
    {
        yield return new WaitWhile(() => _bowman.AIpaused);

        if (_sattisfactionAttempts <= 0) yield break;

        yield return new WaitWhile(() => _bowman.AIpaused);
        LayerMask layerMask = 8;
        _sattisfactionAttempts = 5;
        while (_sattisfactionAttempts > 0)
        {
            yield return new WaitWhile(() => _bowman.AIpaused);
            if (Physics.Raycast(
                transform.position,
                GameManager.Main.GetRegisteredPlayers().First().transform.position,
                Mathf.Infinity,
                layerMask))
            {
                yield return new WaitWhile(() => _bowman.AIpaused);
                GetTargetPoint();
                _isMoving = false;
                _isTryingToSatisfy = false;
                _bowman.ChangeStatus(Bowmanstatus.Shooting);

                yield return new WaitWhile(() => _bowman.AIpaused);

                yield break; // eventuel wird hier nicht gebreakt
            }

            yield return new WaitWhile(() => _bowman.AIpaused);
            _sattisfactionAttempts--;

            yield return new WaitWhile(() => _bowman.AIpaused);
            yield return new WaitForSeconds(1);
            yield return new WaitWhile(() => _bowman.AIpaused);
        }

        _isMoving = false;
        _isTryingToSatisfy = false;
        _bowman.ChangeStatus(Bowmanstatus.Shooting);

        yield return null;
    }
    private IEnumerator SearchRoutine()
    {
        yield return new WaitWhile(() => _bowman.AIpaused);
        yield return new WaitForSeconds(searchTime);
        yield return new WaitWhile(() => _bowman.AIpaused);
        if (Vector3.Distance(transform.position, _player.transform.position) < playerToCloseDistance)
        {
            if (!GameManager.Main.Enemies_GameObjects.Where(g => g != gameObject).Any())
            {
                yield return new WaitWhile(() => _bowman.AIpaused);
                _bowman.ChangeStatus(Bowmanstatus.Panick);

                yield return new WaitWhile(() => _bowman.AIpaused);

                yield break;
            }

            yield return new WaitWhile(() => _bowman.AIpaused);
            _bowman.ChangeStatus(Bowmanstatus.Following);

            yield return new WaitWhile(() => _bowman.AIpaused);

            yield break;
        }

        yield return new WaitWhile(() => _bowman.AIpaused);
        _bowman.ChangeStatus(Bowmanstatus.Shooting);

        yield return new WaitWhile(() => _bowman.AIpaused);
    }
    private void TryToSattisfy()
    {
        if (_isTryingToSatisfy) return;
        _isTryingToSatisfy = true;
        StartCoroutine(SatisfyCoroutine());
    }
#endregion
}