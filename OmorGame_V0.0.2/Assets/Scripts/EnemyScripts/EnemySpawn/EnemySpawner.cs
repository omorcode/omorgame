﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;


public class EnemySpawner : MonoBehaviour
{
#region Fields
    public SpawnerState currentState = SpawnerState.Spawning;
    [Tooltip("If you dont set enough spawnpoints or enemies the first value of the list will be used")]
    public Transform[] enemySpawnPoints;
    public bool isRushHourSpawner;
    public RoomManager parentRoom;
    public bool SpawnerIsFished;
    public EnemyWave[] waves;
    private bool _canSpawn;
    private int _currentWaveNumber;
    private int _enemyToSpawnNumber;
    private float _nextSpawnTime;
    private EnemyWave _currentWave;
    [ShowInInspector] private List<GameObject> _enemiesOfThisWave = new List<GameObject>();
#endregion


#region Public Methods
    public void ChangeSpawnerState(SpawnerState newState)
    {
        switch (newState)
        {
            case SpawnerState.Spawning:
                break;
            case SpawnerState.Pausing:
                break;
            case SpawnerState.WaveFinished:
                if (_currentWaveNumber + 1 == waves.Length)
                {
                    ChangeSpawnerState(SpawnerState.SpawnerFinished);

                    break;
                }

                parentRoom.NewWaveCheck();

                break;
            case SpawnerState.SpawnerFinished:
            {
                SpawnerIsFished = true;
                parentRoom.AllSpawnerFinisheCheck();
            }

                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
        }

        currentState = newState;
    }
    public void RegisterEnemy(GameObject enemy)
    {
        _enemiesOfThisWave.Register(enemy);
        GameManager.Main.RegisterEnemy(enemy);
    }
    [Button]
    public void ResetSpawner()
    {
        _enemiesOfThisWave.ForEach(e => Destroy(e));
        _enemiesOfThisWave.Clear();
        _currentWaveNumber = 0;
        GetWavesValues();
    }
    [Button]
    public void StartTheSpawning()
    {
        _canSpawn = true;
    }
    public void UnregisterEnemy(GameObject enemy)
    {
        _enemiesOfThisWave.Unregister(enemy);
        GameManager.Main.UnregisterEnemy(enemy);
        if (_enemiesOfThisWave.Count == 0) ChangeSpawnerState(SpawnerState.WaveFinished);
    }
#endregion


#region Private Methods
    private void Awake()
    {
        parentRoom = GetComponentInParent<RoomManager>();
        parentRoom.RegisterEnemySpawner(gameObject);
        GetWavesValues();
    }
    private void GetWavesValues()
    {
        _currentWave = waves[_currentWaveNumber];
        _enemyToSpawnNumber = _currentWave.typeOfEnemies.Length;
        _nextSpawnTime = _currentWave.spawnInterval;
    }
    private void OnEnable()
    {
        // gets registered to the Gamemanager once it is enabled  
    }
    private void SpawnWave()
    {
        if (_canSpawn && _nextSpawnTime < Time.time)
        {
            var randomEnemy = _currentWave.typeOfEnemies[Mathf.Max(
                _currentWave.typeOfEnemies.Length - _enemyToSpawnNumber,
                0)];

            var randomPoint =
                enemySpawnPoints[Mathf.Max(_currentWave.typeOfEnemies.Length - _enemyToSpawnNumber, 0)];

            var enemy = Instantiate(randomEnemy, randomPoint.position, randomEnemy.transform.rotation);
            enemy.GetComponent<Enemy>().SetSpawner(this);
            // registriert diesen Spawner im ENEMIE
            _enemyToSpawnNumber--;
            RegisterEnemy(enemy);
            _nextSpawnTime = _nextSpawnTime + _currentWave.spawnInterval;
            if (_enemyToSpawnNumber == 0) _canSpawn = false;
        }
    }
    private void Update()
    {
        SpawnWave();
        //GameObject[] totalEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        //uff das braucht vie speicherdas muss nicht im update passieren 
        if (parentRoom.AllowNextWave && !_canSpawn && _currentWaveNumber + 1 != waves.Length)
        {
            _currentWaveNumber++;
            _canSpawn = true;
            GetWavesValues();
        }
    }
#endregion
}
public enum SpawnerState
{
    Spawning,
    Pausing,
    WaveFinished,
    SpawnerFinished
}