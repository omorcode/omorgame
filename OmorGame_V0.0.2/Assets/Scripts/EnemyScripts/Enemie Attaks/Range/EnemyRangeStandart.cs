﻿using Sirenix.OdinInspector;

public class EnemyRangeStandart : AB_Directional
{
    #region Public Fields + Properties

    [HideIf(nameof(hideFromGamedesigners)), TabGroup("Attack Values"), PropertyOrder(1f), PropertyRange(1f, 50f)]
    public float playerDetectionRange;

    #endregion Public Fields + Properties

    #region Protected Methods

    [Button]
    protected override void Action()
    {
        base.Action();
        //if (FindDestroyableObject(transform.parent.transform.position, playerDetectionRange))
        //{
        //    direction = -(transform.position - opponents[0].gameObject.transform.localPosition).normalized;
        //    base.Action();
        //}
        /*
         * TODO :
         * Fix this in the add Gamemanager Fuction
         */
    }

    #endregion Protected Methods

    #region Private Methods


    #endregion Private Methods
}