﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;


public abstract class GeneralEnemyAttack : MonoBehaviour
{
#region Fields
    [ShowInInspector] public EnemyStandartMeele enemyMeeleAttack;
    [ShowInInspector] public EnemyRangeStandart enemyRangeAttack;
    private bool _canMeeleAttack;
#endregion


#region Protected Methods
    /// <summary>
    ///     if a player is in attackin range he shall be shot
    /// </summary>
    protected void DoAttackRange()
    {
        enemyRangeAttack.Attack();
    }
    /// <summary>
    ///     if the enemy is allowd to atteck (Meele) he will
    /// </summary>
    protected void DoAttackMeele()
    {
        if (_canMeeleAttack) enemyMeeleAttack.Attack();
    }
#endregion


#region Private Methods
    private void OnCollisionEnter2D(Collision2D other)
    {
        DoAttackMeele(); 
    }
    /// <summary>
    ///     the possible attack shall be executed
    /// </summary>
    private void Update()
    {
        attackEvent?.Invoke();
    }
#endregion


    protected event Action attackEvent;
}