﻿using UnityEngine;


public class DamageToPlayerOnTouch : MonoBehaviour
{
    #region Fields
    private GeneralEnemyMovementFollowPlayer _followPlayer;
    #endregion


    #region Private Methods
    private void Awake()
    {
        _followPlayer = gameObject.GetComponent<GeneralEnemyMovementFollowPlayer>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>())
        {
            collision.gameObject.GetComponent<ILifeController>().DoDmg(3);
            _followPlayer.Moving = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        _followPlayer.Moving = true;
    }
    #endregion
}