using UnityEngine;


public class Corpse : MonoBehaviour
{
#region Fields
    public RoomManager Room;
#endregion


#region Private Methods
    private void OnDestroy()
    {
        Room.UnregisterCorpse(gameObject);
    }
#endregion
}