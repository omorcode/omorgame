﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;


public class EnemyLifeController : MonoBehaviour, ILifeController
{
#region Fields
    public Sprite Dead;
    public UnityEvent deadEvent;
    [SerializeField] public bool destroyIfZeroLife;
    public Enemy enemy;
    public bool isDying;
    [ShowInInspector] [LabelText("private lifepoint")]
    public float lifepoints;
    public UnityEvent preDeadEvent;
    [SerializeField] protected float maxLifepoints;
    private EnemyStatusController _enemyStatusController;
#endregion


#region Properies
    [ShowInInspector] public Dictionary<Element, float> ResistanceMultiplier { get; set; }
#endregion


#region Constructor
    public EnemyLifeController()
    {
        ResistanceMultiplier = new Dictionary<Element, float>();
        for (var i = 0; i < Enum.GetValues(typeof(Element)).Length; i++) ResistanceMultiplier.Add((Element) i, 1);
    }
#endregion


#region Public Methods
    public void DestroyThis()
    {
        Destroy(gameObject);
    }
    public IEnumerator OnHit()
    {
        if (enemy.enemyMovement == null) yield break;
        enemy.animator.SetTrigger("Hit");
        var temp = enemy.enemyMovement.IsMoving;
        enemy.enemyMovement.IsMoving = false;

        yield return new WaitUntil(() => enemy.animator.GetCurrentAnimatorStateInfo(0).IsName("OnHit"));
        enemy.enemyMovement.IsMoving = temp;
    }
    public float ResistanceChecker(float dmg, Element element)
    {
        return dmg * ResistanceMultiplier[element];
    }
    public void SpawnCarcas(GameObject pfbDeadEnemie)
    {
        var clone = Instantiate(pfbDeadEnemie, transform.position, Quaternion.identity);
        enemy.Spawner.parentRoom.RegisterCorpse(clone);
        if (clone.IsOverCanyon()) clone.DeastroyAfterTime(0.5f, clone.GetComponent<Corpse>());
    }
#endregion


#region Private Methods
    private void Start()
    {
        Lifepoints = MaxLifepoints;
        _enemyStatusController = gameObject.GetComponent<EnemyStatusController>();
    }
    private IEnumerator WhenDead()
    {
        if (destroyIfZeroLife)
        {
            deadEvent.Invoke();
            DestroyThis();

            yield break;
        }

        yield return new WaitForEndOfFrame();
        if (enemy.Spawner != null) enemy.Spawner.UnregisterEnemy(gameObject);
        preDeadEvent?.Invoke();
        if (enemy.enemyRigidbody2D != null) enemy.enemyRigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;
        gameObject.layer = 16;

        yield return new WaitUntil(() => enemy.animator.GetCurrentAnimatorStateInfo(0).IsName("Dying"));
        yield return new WaitWhile(() => isDying);
        deadEvent.Invoke();
    }
#endregion


    public bool Damageable { get; set; }
    [ShowInInspector]
    public float Lifepoints
    {
        get => lifepoints;
        set
        {
            lifepoints = value;
            if (lifepoints <= 0) StartCoroutine(WhenDead());
        }
    }
    public float MaxLifepoints
    {
        get => maxLifepoints;
        set => maxLifepoints = value;
    }
    public void DoDmg(float amount, Element element = Element.Normal)
    {
        amount = ResistanceChecker(amount, element);
        StartCoroutine(OnHit());
        switch (element)
        {
            case Element.Normal:
                Lifepoints -= amount;

                break;
            case Element.Piercing:
                _enemyStatusController.DoStatusEffect(EnemyStatusEffect.Bleed, amount);
                Lifepoints -= amount;

                break;
            case Element.Explosion:
                Lifepoints -= amount;

                break;
            case Element.Slime:
                _enemyStatusController.DoStatusEffect(EnemyStatusEffect.Sticky, amount / 10);

                break;
            case Element.Fire:
                _enemyStatusController.DoStatusEffect(EnemyStatusEffect.Burn, amount);

                break;
            case Element.Water:
                Lifepoints -= amount;

                break;
            case Element.Ice:
                _enemyStatusController.DoStatusEffect(EnemyStatusEffect.Freeze, amount);
                Lifepoints -= amount;

                break;
            case Element.Lightning:
                _enemyStatusController.DoStatusEffect(EnemyStatusEffect.Blitzed, amount / 100);
                Lifepoints -= amount;

                break;
            case Element.Knockback:
                _enemyStatusController.DoStatusEffect(EnemyStatusEffect.Knockback, amount / 750);

                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(element), element, null);
        }
    }
    public void Heal(float amount)
    {
        Lifepoints += amount;
    }
}