﻿using Sirenix.OdinInspector;
using UnityEngine;


[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(SpawnCoin))]
[RequireComponent(typeof(StunnAndKnockback))]
[RequireComponent(typeof(Rigidbody2D), typeof(EnemyLifeController), typeof(EnemyStatusController))]
[RequireComponent(typeof(CapsuleCollider2D), typeof(EnemyLifeController))]
public class Enemy : DamageableByPlayer
{
#region Fields
    [ShowIf(nameof(programmerMode))] public bool AIpaused;
    public Animator animator;
    [ShowIf(nameof(programmerMode))] public CapsuleCollider2D enemyCapsuleCollider;
    [ShowIf(nameof(programmerMode))] public EnemyLifeController enemyLifeController;
    [Required] [ShowIf(nameof(programmerMode))]
    public EnemyMovementController enemyMovement;
    [ShowIf(nameof(programmerMode))] [ShowIf(nameof(programmerMode))]
    public Rigidbody2D enemyRigidbody2D;
    [ShowIf(nameof(programmerMode))] public EnemyStatusController enemyStatusController;
    [Required] [ShowIf(nameof(programmerMode))]
    public GeneralEnemyAttack generalEnemyAttack;
    public bool programmerMode;
    public SpawnCoin SpawnCoin;
    [ShowIf(nameof(programmerMode))] public StunnAndKnockback stunnAndKnockback;
#endregion


#region Properies
    [ShowIf(nameof(programmerMode))] public EnemySpawner Spawner { get; private set; }
#endregion


#region Public Methods
    public void SetSpawner(EnemySpawner spawner)
    {
        if (Spawner == null)
        {
            Spawner = spawner;

            return;
        }

        Debug.LogError("You tried to set the Spawner of an Enemy but it was already done! This should not happen!!!");
    }
#endregion


#region Private Methods
    private void Awake()
    {
        enemyLifeController = GetComponent<EnemyLifeController>();
        enemyRigidbody2D = GetComponent<Rigidbody2D>();
        enemyCapsuleCollider = GetComponent<CapsuleCollider2D>();
        generalEnemyAttack = GetComponent<GeneralEnemyAttack>();
        enemyLifeController = GetComponent<EnemyLifeController>();
        enemyStatusController = GetComponent<EnemyStatusController>();
        stunnAndKnockback = GetComponent<StunnAndKnockback>();
        enemyLifeController.enemy = this;
        SpawnCoin = GetComponent<SpawnCoin>();
        SpawnCoin.enemy = this;
        animator = GetComponent<Animator>();
        enemyMovement = GetComponent<EnemyMovementController>();

        if (enemyMovement == null) return;
        enemyMovement.enemy = this;
    }
#endregion


    /* TODO :
     * - When enemy dies it should call the unregister from its spawner
     * - and it shall unregister from Gamemanger
     */
}