using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StabStabWoof_Attack : MonoBehaviour
{
#region Fields
    public float cooldown;
    public int[] dmg;
    public List<GameObject> dmgreciever = new List<GameObject>();
    public Enemy enemy;
    private StabStabWoof _stabStabWoof;
#endregion


#region Public Methods
    public void Stab1(int stabnumber = 0)
    {
        StartCoroutine(
            waitBeforeStauChange(enemy.animator.GetCurrentAnimatorClipInfo(0).Length, StabStabWoofStatus.Woof));
    }
    public void Stab2(int stabnumber = 1)
    {
        Debug.Log("stab2");
        dmgreciever.ForEach(d => d.GetComponent<ILifeController>().DoDmg(dmg[1]));
        StartCoroutine(
            waitBeforeStauChange(enemy.animator.GetCurrentAnimatorClipInfo(0).Length, StabStabWoofStatus.Woof));
    }
#endregion


#region Private Methods
    //private IEnumerator DamageOverTime(GameObject reciever)
    //{
    //    ILifeController lc = reciever.gameObject.GetComponent<ILifeController>();
    //    while (dmgreciever.Contains(reciever.gameObject))
    //    {
    //        lc.DoDmg(dmg);
    //        yield return new WaitForSeconds(cooldown);
    //    }
    //}
    private void Awake()
    {
        _stabStabWoof = gameObject.GetComponentInParent<StabStabWoof>();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent(typeof(ILifeController)) && !dmgreciever.Contains(other.gameObject))
            dmgreciever.Add(other.gameObject);

        //StartCoroutine(DamageOverTime(other.gameObject));
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (dmgreciever.Contains(other.gameObject)) dmgreciever.Remove(other.gameObject);
    }
    private IEnumerator waitBeforeStauChange(float waitTime, StabStabWoofStatus folgeStatus)
    {
        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        yield return new WaitForSeconds(waitTime / 2);
        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        dmgreciever.ForEach(d => d.GetComponent<ILifeController>().DoDmg(dmg[0]));

        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        yield return new WaitForSeconds(waitTime / 2);
        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        _stabStabWoof.ChangeStatus(folgeStatus);
    }
#endregion
}