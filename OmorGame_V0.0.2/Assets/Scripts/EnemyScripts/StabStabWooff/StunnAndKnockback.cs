using System.Collections;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;


[RequireComponent(typeof(Rigidbody2D))]
public class StunnAndKnockback : MonoBehaviour
{
#region Fields
    public Vector2 _direction;
    [ShowIf("programmerMode")] public UnityEvent DeaktivateEvent;
    [ShowIf("programmerMode")] public float debugStunnTime;
    [ShowIf("programmerMode")] public float debugYeetSpeed;
    [ShowIf("programmerMode")] public float debugYeetTime;
    public bool programmerMode;
    [ShowIf("programmerMode")] public UnityEvent ReaktivateEvent;
    private Rigidbody2D _GO_Player_rigidbody2D;
    private Rigidbody2D _this_rigidbody2D;
    private bool _isMoving;
    private bool _isStunned;
#endregion


#region Public Methods
    public void DebugStunn()
    {
        Stunn(debugStunnTime);
    }
    public void Stunn(float stunnTime)
    {
        StartCoroutine(StunnRoutine(stunnTime));
    }
    /// <summary>
    ///     be carefull this requires manual ustunn
    /// </summary>
    public void Stunn()
    {
        _isStunned = true;
        StartCoroutine(StunnRoutine());
    }
    public void Unstunn()
    {
        _isStunned = false;
    }
    [Button]
    public void Yeet(float yeetTime, float yeetSpeed)
    {
        _GO_Player_rigidbody2D = GameManager.Main.GetRegisteredPlayers().First().GetComponent<Rigidbody2D>();
        _this_rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
        _direction = -(_GO_Player_rigidbody2D.position - _this_rigidbody2D.position).normalized * yeetSpeed;
        DeaktivateEvent.Invoke();
        StartCoroutine(YeetRoutine(yeetTime));
    }
    public void Yeet()
    {
        Yeet(debugYeetTime, debugYeetSpeed);
    }
#endregion


#region Private Methods
    private void Awake()
    {
        _this_rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
    }
    private void FixedUpdate()
    {
        if (!_isMoving) return;
        _this_rigidbody2D.MovePosition(_this_rigidbody2D.position + _direction * Time.fixedDeltaTime);
    }
    private IEnumerator StunnRoutine(float st)
    {
        DeaktivateEvent.Invoke();
        _this_rigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;

        yield return new WaitForSeconds(st);
        _this_rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
        ReaktivateEvent.Invoke();
    }
    private IEnumerator StunnRoutine()
    {
        DeaktivateEvent.Invoke();
        _this_rigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;

        yield return new WaitWhile(() => _isStunned);
        _this_rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
        ReaktivateEvent.Invoke();
    }
    private IEnumerator YeetRoutine(float yt)
    {
        _isMoving = true;

        yield return new WaitForSeconds(yt);
        _isMoving = false;
        ReaktivateEvent?.Invoke();
    }
#endregion
}