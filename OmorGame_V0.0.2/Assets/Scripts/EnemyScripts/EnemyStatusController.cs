﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;


public class EnemyStatusController : MonoBehaviour
{
#region Fields
    public float betweenBleedingInSec;
    public float betweenBurningInSec = 1f;
    public int bleedingTicks;
    public List<int> bleedTickTimers = new List<int>();
    public int burningTicks;
    public List<int> burnTickTimers = new List<int>();
    public EnemyLifeController enemyLifeController;
    public float fireDamage;
    [ShowInInspector] private uint _activeEffects;
    private readonly uint _bleedBitMask = 0b_0010;
    private readonly uint _burnBitMask = 0b_0100;
    private readonly uint _freezeBitMask = 0b_0001;
    private readonly uint _stickBitMask = 0b_1000;
    private float _totalstickytime;
    private float _totalfreezetime;
    private Enemy thisEnemy;
#endregion


#region Properies
    public EnemyAliveStatus EnemyAliveStatus { get; }
#endregion


#region Public Methods
    public void ApplyStatusEffectBleeding(float damage)
    {
        DOTCoroutineExecuter(bleedingTicks, betweenBleedingInSec, damage, bleedTickTimers, _bleedBitMask);
    }
    public void ApplyStatusEffectBurning(float x)
    {
        DOTCoroutineExecuter(Mathf.RoundToInt(x), betweenBurningInSec, fireDamage, burnTickTimers, _burnBitMask);
    }
    public void ApplyStatusEffectFreeze(float x)
    {
        _totalfreezetime += x;
        if ((_activeEffects & _freezeBitMask) != _freezeBitMask) StartCoroutine(FreezeRoutine(x));
        RegisterEffect(_freezeBitMask);
    }
    public void ApplyStatusEffectSticky()
    {
        _activeEffects |= _stickBitMask;
    }
    public void ApplyStatusEffectSticky(float x)
    {
        _totalstickytime += x;
        if ((_activeEffects & _stickBitMask) != _stickBitMask) StartCoroutine(StickyRoutine(x));
        RegisterEffect(_stickBitMask);
    }
    public void ChangeEnemyStatus(EnemyAliveStatus newState)
    {
        switch (newState)
        {
            case EnemyAliveStatus.Alive:
                break;
            case EnemyAliveStatus.Paused:
                break;
            case EnemyAliveStatus.Dead:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
        }
    }
    public IEnumerator DoDamageOverTimeRoutine(float time, float damage, List<int> TickTimers, uint bitmask)
    {
        while (TickTimers.Count > 0)
        {
            for (var i = 0; i < TickTimers.Count; i++) TickTimers[i]--;
            enemyLifeController.DoDmg(damage);
            TickTimers.RemoveAll(i => i == 0);

            yield return new WaitForSeconds(time);
        }

        UnregisterEffect(bitmask);
    }
    /// <summary>
    ///     x is the value that changes stuff according to the GDD please read there
    /// </summary>
    /// <param name="newStatusEffect"></param>
    /// <param name="x"></param>
    public void DoStatusEffect(EnemyStatusEffect newStatusEffect, float x)
    {
        switch (newStatusEffect)
        {
            case EnemyStatusEffect.Sticky:
                ApplyStatusEffectSticky(x);

                break;
            case EnemyStatusEffect.Freeze:
                ApplyStatusEffectFreeze(x);

                break;
            case EnemyStatusEffect.Bleed:
                ApplyStatusEffectBleeding(x);

                break;
            case EnemyStatusEffect.Burn:
                ApplyStatusEffectBurning(x);

                break;
            case EnemyStatusEffect.None:
                break;
            case EnemyStatusEffect.Blitzed:
                ApplyStatusEffectBlitzed(x);

                break;
            case EnemyStatusEffect.Knockback:
                ApplyStatusEffectKnockback(x);

                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newStatusEffect), newStatusEffect, null);
        }
    }
    public void DOTCoroutineExecuter(int ticks, float time, float damage, List<int> TickTimers, uint bitmask)
    {
        if (!CheckActiveEffects(bitmask))
        {
            TickTimers.Add(ticks);
            StartCoroutine(DoDamageOverTimeRoutine(time, damage, TickTimers, bitmask));
        }
        else
        {
            TickTimers.Add(ticks);
        }

        RegisterEffect(bitmask);
    }
#endregion


#region Private Methods
    private void ApplyStatusEffectBlitzed(float stunTime)
    {
        thisEnemy.stunnAndKnockback.Stunn(stunTime);
    }
    private void ApplyStatusEffectKnockback(float yeetTime)
    {
        thisEnemy.stunnAndKnockback.Yeet(yeetTime, 10f);
    }
    private void Awake()
    {
        thisEnemy = GetComponent<Enemy>();
        enemyLifeController = gameObject.GetComponent<EnemyLifeController>();
        fireDamage = 1;
        betweenBurningInSec = 0.1f;
    }
    private bool CheckActiveEffects(uint bitMask)
    {
        return (_activeEffects & bitMask) == bitMask;
    }
    private void DisableMovement()
    {
        gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
    }
    private void Freeze()
    {
        DisableMovement();
    }
    private IEnumerator FreezeRoutine(float freezeTime)
    {
        Freeze();
        //TODO: Enemy mini Manager ... change script according to this.
        gameObject.GetComponent<GeneralEnemyAttack>().enabled = false;
        while (_totalfreezetime > 0)
        {
            yield return new WaitForSeconds(freezeTime);
            _totalfreezetime -= freezeTime;
        }

        UnFreeze();
    }
    private void RegisterEffect(uint bitmask)
    {
        _activeEffects |= bitmask;
    }
    private void Sticky(float stickytime)
    {
        DisableMovement();
        _totalstickytime += stickytime;
    }
    private IEnumerator StickyRoutine(float stickytime)
    {
        Sticky(stickytime);
        while (_totalstickytime > 0)
        {
            yield return new WaitForSeconds(stickytime);
            _totalstickytime -= stickytime;
        }

        UnSticky();
    }
    private void UnFreeze()
    {
        gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        _totalfreezetime = 0;
        UnregisterEffect(_freezeBitMask);
    }
    private void UnregisterEffect(uint bitmask)
    {
        _activeEffects ^= bitmask;
    }
    private void UnSticky()
    {
        gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        UnregisterEffect(_stickBitMask);
    }
#endregion
}
public enum EnemyStatusEffect
{
    None,
    Sticky,
    Freeze,
    Bleed,
    Burn,
    Blitzed,
    Knockback
}
public enum EnemyAliveStatus
{
    Alive,
    Paused,
    Dead
}