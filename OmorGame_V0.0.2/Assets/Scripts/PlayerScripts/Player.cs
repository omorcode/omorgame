﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;


public class Player : MonoBehaviour
{
#region Fields
    public Animator animator;
    public GameManager gameManager;
    public float speed;
    private Rigidbody2D myRigidbody;
    //private Animator animator;
    private Vector3 change;
#endregion


#region Properies
    public Interact Interact { get; private set; }
    public Inventory InventoryCo { get; private set; }
    public PlayerAttack PlayerAttackCO { get; private set; }
    public PlayerAttributes PlayerAttributesCO { get; private set; }
    public Rigidbody2D PlayeRigidbody2D { get; private set; }
    public PlayerInput playerInput { get; private set; }
    public PlayerLifeController PlayerLifeControllerCO { get; private set; }
    public PlayerMoveController PlayerMoveControllerCO { get; private set; }
    public PlayerUIController PlayerUiController { get; private set; }
    public UsePotions UsePotions { get; private set; }
#endregion


#region Public Methods
    public bool DoExplosionDamage(GameObject receiver, float amount)
    {
        return DoAttributalyCalculatedDamage(receiver, amount, Element.Explosion);
    }
    public bool DoFireDamage(GameObject receiver, float amount)
    {
        return DoAttributalyCalculatedDamage(receiver, amount, Element.Fire);
    }
    public bool DoIceDamage(GameObject receiver, float amount)
    {
        return DoAttributalyCalculatedDamage(receiver, amount, Element.Ice);
    }
    public bool DoKnockbackDamage(GameObject receiver, float amount)
    {
        return DoAttributalyCalculatedDamage(receiver, amount, Element.Knockback);
    }
    public bool DoLightningDamage(GameObject receiver, float amount)
    {
        return DoAttributalyCalculatedDamage(receiver, amount, Element.Lightning);
    }
    public bool DoNormalDamage(GameObject receiver, float amount)
    {
        return DoAttributalyCalculatedDamage(receiver, amount);
    }
    public bool DoPiercingDamage(GameObject receiver, float amount)
    {
        return DoAttributalyCalculatedDamage(receiver, amount, Element.Piercing);
    }
    public bool DoSlimeDamage(GameObject receiver, float amount)
    {
        return DoAttributalyCalculatedDamage(receiver, amount, Element.Slime);
    }
    public bool DoWaterDamage(GameObject receiver, float amount)
    {
        return DoAttributalyCalculatedDamage(receiver, amount, Element.Water);
    }
#endregion


#region Private Methods
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        PlayerAttackCO = gameObject.GetComponent<PlayerAttack>();
        PlayerAttributesCO = gameObject.GetComponent<PlayerAttributes>();
        PlayerLifeControllerCO = gameObject.GetComponent<PlayerLifeController>();
        PlayerMoveControllerCO = GetComponent<PlayerMoveController>();
        InventoryCo = GetComponent<Inventory>();
        animator = GetComponent<Animator>();
        PlayeRigidbody2D = GetComponent<Rigidbody2D>();
        PlayerMoveControllerCO.player = this;
        Interact = GetComponent<Interact>();
        Interact.player = this;
        playerInput = GetComponent<PlayerInput>();
        GameManager.Main.RegisterPlayer(gameObject);
        PlayerUiController = GetComponent<PlayerUIController>();
        UsePotions = GetComponent<UsePotions>();
        UsePotions.player = this;
        PlayerUiController.player = this;
    }
    private bool DoAttributalyCalculatedDamage(GameObject receiver, float amount, Element element = Element.Normal)
    {
        if (receiver.GetComponent(typeof(ILifeController)) is ILifeController receiverLifeController)
        {
            receiverLifeController.DoDmg(PlayerAttributesCO.ApplyElementMultiplier(amount, element), element);

            return true;
        }

        return false;
    }
    //private void MoveCharacter()
    //{
    //    myRigidbody.MovePosition(
    //        transform.position + change * speed * Time.deltaTime
    //    );
    //}
    private void OnDestroy()
    {
        GameManager.Main.RemovePlayer(gameObject);
    }
    private void Start()
    {
        playerInput.uiInputModule = UIManager.Main.GetComponent<InputSystemUIInputModule>();
    }
    private void Update()
    {
        //change = Vector3.zero;
        //change.x = Input.GetAxisRaw("Horizontal");
        //change.y = Input.GetAxisRaw("Vertical");
        //UpdateAnimationAndMove();
    }
    private void UpdateAnimationAndMove()
    {
        if (change != Vector3.zero)
        {
            // MoveCharacter();
            animator.SetFloat("moveX", change.x);
            animator.SetFloat("moveY", change.y);
            animator.SetBool("moving", true);
        }
        else
        {
            animator.SetBool("moving", false);
        }
    }
#endregion
}