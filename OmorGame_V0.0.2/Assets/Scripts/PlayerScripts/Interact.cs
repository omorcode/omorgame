using UnityEngine;


public class Interact : MonoBehaviour
{
#region Fields
    public Player player;
#endregion


#region Public Methods
    public void TryToInteract()
    {
        var hit2D = Physics2D.Raycast(
            transform.position,
            player.PlayerMoveControllerCO.currentCardinal,
            3,
            LayerMask.GetMask(LayerMask.LayerToName(23)));

        if (hit2D) hit2D.transform.gameObject.GetComponent<Socializeable>().InteractionStart?.Invoke(gameObject);
    }
#endregion
}