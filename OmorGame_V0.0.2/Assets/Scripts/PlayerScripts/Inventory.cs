﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;


public class Inventory : MonoBehaviour
{
#region Fields
    public UnityEvent addCoins;
    public int Coins;
    public List<Item_SOS> debugItem = new List<Item_SOS>();
    [FormerlySerializedAs("itemToUse")] [InlineButton(nameof(DebugUse))] [PropertyRange(1, "$_inventoryCount")]
    public int debugItemToUse;
    public UnityEvent InventoryChanged;
    [ProgressBar(0, "$maxInventorySize", Segmented = true)] [HideLabel]
    public int inventoryFullness;
    [Range(1, 100)] public int maxInventorySize = 15;
    [ShowInInspector] public GameObject player;
    [ShowInInspector] public List<InventorySlot> PlayerInventory = new List<InventorySlot>();
    private float _inventoryCount;
    [ShowInInspector] [ReadOnly] private bool _isFull;
    [ShowInInspector] private List<PotionDurabilitypair> _activePotions = new List<PotionDurabilitypair>();
    [ShowInInspector] private PotionDurabilitypair _activeCocktail;
    [ShowInInspector] private List<int> _durabilities = new List<int>();
#endregion


#region Public Methods
    public void AddItem(Item_SOS item)
    {
        if (item is Coin_SOS coin)
        {
            Coins += coin.Value;
            addCoins?.Invoke();

            return;
        }

        foreach (var s in PlayerInventory)
            if (s.item == item)
            {
                s.amount++;
                InventoryChanged.Invoke();

                return;
            }

        PlayerInventory.Add(new InventorySlot(item));
        InventoryChanged.Invoke();
    }
    public void AddItem(List<Item_SOS> itemlist)
    {
        itemlist.ForEach(i => AddItem(i));
    }
    /// <summary>
    ///     add this to the finished room unity event
    /// </summary>
    [Button]
    public void FinishedRoom()
    {
        RoomDurabilityCheck();
        DecreaseRoomDurabillitiy();
        RoomDurabilityCheck();
    }
    public bool RemoveCoins(int amount)
    {
        if (Coins - amount >= 0)
        {
            Coins -= amount;
            addCoins?.Invoke();

            return true;
        }

        return false;
    }
    public void RoomDurabilityCheck()
    {
        if (_activePotions.Count != 0)
        {
            var potionsToDelete = new List<PotionDurabilitypair>();
            potionsToDelete.AddRange(_activePotions.Where(e => e.durability == 0));
            potionsToDelete.ForEach(p => p.potionStyle.Deactivate(player));
            potionsToDelete.ForEach(p => _activePotions.Remove(p));
        }

        if (_activeCocktail != null && _activeCocktail.durability == 0)
        {
            _activeCocktail.potionStyle.Deactivate(player);
            _activeCocktail = null;
        }
    }
    public void Use(int inventorySpace)
    {
        if (inventorySpace > PlayerInventory.Count - 1 && inventorySpace > 0) return;
        PlayerInventory[inventorySpace].item.Use(player);
        PlayerInventory[inventorySpace].amount--;
        if (PlayerInventory[inventorySpace].item is Shot_SOS potionToUse && potionToUse.hasRoomDurability)
            _activePotions.Add(new PotionDurabilitypair(potionToUse, potionToUse.roomDurability));

        if (PlayerInventory[inventorySpace].item is Cocktail_SOS newActiveCocktail)
            _activeCocktail = new PotionDurabilitypair(newActiveCocktail, newActiveCocktail.roomDurability);

        if (PlayerInventory[inventorySpace].amount == 0) PlayerInventory.RemoveAt(inventorySpace);
        InventoryChanged.Invoke();
    }
    public void Use(InventorySlot slot)
    {
        Debug.Log("i am real " + gameObject);
        slot.item.Use(player);
        slot.amount--;
        if (slot.item is Shot_SOS potionToUse && potionToUse.hasRoomDurability)
            _activePotions.Add(new PotionDurabilitypair(potionToUse, potionToUse.roomDurability));

        if (slot.item is Cocktail_SOS newActiveCocktail)
            _activeCocktail = new PotionDurabilitypair(newActiveCocktail, newActiveCocktail.roomDurability);

        InventoryChanged.Invoke();
        if (slot.amount == 0) PlayerInventory.Remove(slot);
    }
#endregion


#region Private Methods
    private void Awake()
    {
        player = gameObject;
        Coins = 0;
    }
    [Button]
    private void DebugAddItem()
    {
        AddItem(debugItem);
    }
    private void DebugUse()
    {
        // hier müsste noch ein code hin zun entfernen wenn es ein Potion oder ein Collectabel ist;
        Use(debugItemToUse - 1);
    }
    private void DecreaseRoomDurabillitiy()
    {
        if (_activePotions.Count != 0) _activePotions.ForEach(a => a.durability--);
        if (_activeCocktail != null) _activeCocktail.durability--;
    }
    private void InventoryFull()
    {
        _isFull = true;
        Debug.LogError("Inventory is full. Code something that should happen here !");
    }
#endregion


    private class PotionDurabilitypair
    {
#region Fields
        public int durability;
        public readonly Potion_SOS potionStyle;
#endregion


#region Constructor
        public PotionDurabilitypair(Potion_SOS potionStyle, int durability)
        {
            this.potionStyle = potionStyle;
            this.durability = durability;
        }
#endregion
    }
}
public class InventorySlot
{
#region Fields
    public int amount;
    public Item_SOS item;
#endregion


#region Constructor
    public InventorySlot(Item_SOS _item)
    {
        item = _item;
        amount = 1;
    }
#endregion
}