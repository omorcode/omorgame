﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;


public class PlayerAttributes : MonoBehaviour
{
    public enum Attribute
    {
        Speed,
        Lifetotal,
        Agility,
        AttackMultiplier
    }


#region Fields
    public Action SpeedMultiplierChanged;
    private float _speedMultiplier;
#endregion


#region Properies
    [ShowInInspector] public float AgilityMultiplier { get; set; }
    [ShowInInspector] public float AttackMultiplier { get; set; }
    [ShowInInspector] [ReadOnly] public Dictionary<Element, float> ExtraDmgElementalMultiplier { get; set; }
    [ShowInInspector] public float LifeTotal { get; private set; }
    [ShowInInspector]
    public float SpeedMultiplier
    {
        get => _speedMultiplier;
        private set
        {
            if (_speedMultiplier != value)
            {
                _speedMultiplier = value;
                SpeedMultiplierChanged?.Invoke();
            }
        }
    }
#endregion


#region Constructor
    public PlayerAttributes()
    {
        AgilityMultiplier = 1;
        AttackMultiplier = 1;
        ExtraDmgElementalMultiplier = new Dictionary<Element, float>();
        for (var i = 0; i < Enum.GetValues(typeof(Element)).Length; i++)
            ExtraDmgElementalMultiplier.Add((Element) i, 1);

        LifeTotal = 3;
        SpeedMultiplier = 1;
    }
#endregion


#region Public Methods
    /// <summary>
    ///     Applier the Agility multiplier
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public float ApplyAgilityMultiplier(float input)
    {
        return input * AgilityMultiplier;
    }
    /// <summary>
    ///     applies the Attack multiplier
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public float ApplyAttackMultiplier(float input)
    {
        return input * AttackMultiplier;
    }
    /// <summary>
    ///     Multypliers the Damage with their elemental Multipliers
    /// </summary>
    /// <param name="input"></param>
    /// <param name="element"></param>
    /// <returns></returns>
    public float ApplyElementMultiplier(float input, Element element = Element.Normal)
    {
        return input * ExtraDmgElementalMultiplier[element];
    }
    /// <summary>
    ///     Applier the Speed multiplier
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public float ApplySpeedMultiplier(float input)
    {
        return input * SpeedMultiplier;
    }
    public void ChangeAttribute(Attribute changingAttribute, float additionalMultiplier)
    {
        switch (changingAttribute)
        {
            case Attribute.Speed:
                ChangeSpeedMultiplier(additionalMultiplier);

                break;
            case Attribute.Lifetotal:
                ChangeLifeTotal(additionalMultiplier);

                break;
            case Attribute.Agility:
                ChangeAgilityMultiplier(additionalMultiplier);

                break;
            case Attribute.AttackMultiplier:
                ChangeAttackMultiplier(additionalMultiplier);

                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(changingAttribute), changingAttribute, null);
        }
    }
    public void ChangeElementMultiplier(Element elementsToChange, float extraMultiplier)
    {
        ExtraDmgElementalMultiplier[elementsToChange] = ExtraDmgElementalMultiplier[elementsToChange] + extraMultiplier;

        //int i = 0;
        //foreach (float elementMulti in ResistanceMultiplier.Keys)
        //{
        //    if ((int)elementsToChange == i)
        //    {
        //        ResistanceMultiplier.Remove()
        //    }
        //}
    }
    public void ChangeElementMultiplier(Element[] elementsToChange, float extraMultiplier)
    {
        foreach (var element in elementsToChange) ChangeElementMultiplier(element, extraMultiplier);
    }
    public void ChangeElementMultiplier(Element[] elementsToChange, float[] extraMultiplierList)
    {
        var i = 0;
        foreach (var element in elementsToChange)
        {
            ChangeElementMultiplier(element, extraMultiplierList[i]);
            i++;
        }
    }
#endregion


#region Private Methods
    /// <summary>
    ///     Additively Change Multiplier
    /// </summary>
    /// <param name="multiplier"></param>
    private void ChangeAgilityMultiplier(float multiplier)
    {
        AgilityMultiplier += multiplier;
    }
    /// <summary>
    ///     Additively Change Multiplier
    /// </summary>
    /// <param name="multiplier"></param>
    private void ChangeAttackMultiplier(float multiplier)
    {
        AttackMultiplier += multiplier;
    }
    /// <summary>
    ///     Additively Change Lifetotal
    /// </summary>
    /// <param name="lifeToAdd"></param>
    private void ChangeLifeTotal(float lifeToAdd)
    {
        LifeTotal += lifeToAdd;
    }
    /// <summary>
    ///     Additively Change Multiplier
    /// </summary>
    /// <param name="multiplier"></param>
    private void ChangeSpeedMultiplier(float multiplier)
    {
        AgilityMultiplier += multiplier;
    }
#endregion


    // Logic of Elementd later when dooing new Dmg Systhem;
}