﻿using UnityEngine;


public class UIManager : MonoBehaviour
{
#region Fields
    public static UIManager Main;
    [SerializeField] protected GameObject PausePanel;
    [SerializeField] protected GameObject HUDPanel;
    [SerializeField] protected GameObject InventoryPanel;
    private Player player1;
#endregion


#region Properies
    public UIState State { get; private set; }
#endregion


#region Public Methods
    public void ChangeState(UIState newState)
    {
        DisableAllPanel();
        switch (newState)
        {
            case UIState.Inventory:
                InventoryPanel.SetActive(true);

                break;
            case UIState.HUD:
                HUDPanel.SetActive(true);

                break;
            case UIState.Pause:
                PausePanel.SetActive(true);

                break;
        }

        State = newState;
    }
    public void ChangeStatus(int newstate)
    {
        ChangeState((UIState) newstate);
    }
    public void CloseInventory()
    {
        ChangeState(UIState.HUD);
    }
    public void OpenInventory()
    {
        ChangeState(UIState.Inventory);
    }
#endregion


#region Private Methods
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (Main == null)
            Main = this;
        else if (Main != this) Destroy(this);
    }
    private void DisableAllPanel()
    {
        HUDPanel.SetActive(false);
        PausePanel.SetActive(false);
        InventoryPanel.SetActive(false);
    }
    private void Start()
    {
        ChangeState(UIState.HUD);
        player1 = GameManager.Main.GetRegisteredPlayers()[0].GetComponent<Player>();
    }
#endregion
}
public enum UIState
{
    Inventory,
    Pause,
    HUD
}