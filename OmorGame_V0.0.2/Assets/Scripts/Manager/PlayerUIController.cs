using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerUIController : MonoBehaviour
{
#region Fields
    public Player player;
    private GameObject _UICanvas;
    private UIManager _uiManager;
    private bool freshSwitch;
    private bool isUiOpen;
#endregion


#region Public Methods
    public void CloseInventory(InputAction.CallbackContext context)
    {
        Debug.Log(player.playerInput.currentActionMap);
        Debug.Log(isUiOpen);
        //player.playerInput.StopAllCoroutines();

        if (!isUiOpen) return;
        if (freshSwitch) return;
        Debug.Log("can i close???????????????????");
        if (context.started)
        {
            isUiOpen = false;
            _uiManager.CloseInventory();
            Debug.Log("can i close?");
            player.playerInput.SwitchCurrentActionMap("Player");
            StartCoroutine(wait());
        }
    }
    public void OpenInventory(InputAction.CallbackContext context)
    {
        Debug.Log(player.playerInput.currentActionMap);

        if (isUiOpen) return;
        if (freshSwitch) return;
        if (context.performed)
        {
            isUiOpen = true;
            _uiManager.OpenInventory();
            player.playerInput.SwitchCurrentActionMap("UI");
            StartCoroutine(wait());
        }
    }
#endregion


#region Private Methods
    private void Start()
    {
        _UICanvas = GameManager.Main.uiCanvas;
        _uiManager = UIManager.Main;
    }
    private void Update() { }
    private IEnumerator wait()
    {
        freshSwitch = true;

        yield return new WaitForSeconds(1f);
        freshSwitch = false;
    }
#endregion
}