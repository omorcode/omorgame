﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;


public class GameManager : Singelton<GameManager>
{
#region Fields
    public List<GameObject> _rooms = new List<GameObject>();
    public RoomManager activeRoom;
    public GameObject camera;
    public List<GameObject> Enemies_GameObjects = new List<GameObject>();
    public FourDrinks fourDrinks;
    public GameObject pfbUICanvas;
    public GameObject player;
    public Vector3 Respawnpoint;
    public RoomManager staringoom;
    public GameObject uiCanvas;
    private readonly List<GameObject> _players = new List<GameObject>();
#endregion


#region Properies
    /// <summary>
    ///     set this by using ChangeGameState(GameState new)
    /// </summary>
    public GameState GameState { get; private set; }
#endregion


#region Public Methods
    public void ChangeGameState(GameState newState)
    {
        switch (newState)
        {
            case GameState.Playing:
                UIManager.Main.ChangeState(UIState.HUD);
                Time.timeScale = 1f;

                break;
            case GameState.Pause:
                UIManager.Main.ChangeState(UIState.Pause);
                Time.timeScale = 0f;

                break;
        }

        GameState = newState;
    }
    public void ChangStartingroom(RoomManager room)
    {
        staringoom = room;
        Respawnpoint = new Vector3(
            staringoom.transform.position.x + 14,
            staringoom.transform.position.y - 6,
            0);

        _players[0].transform.position = Respawnpoint;
        Camera.main.gameObject.GetComponent<CameraMovement>().maxPosition = new Vector2(
            GetRegisteredPlayers().First().transform.position.x,
            GetRegisteredPlayers().First().transform.position.y - 2);

        Camera.main.gameObject.GetComponent<CameraMovement>().minPosition =
            Camera.main.gameObject.GetComponent<CameraMovement>().maxPosition;
    }
    public List<GameObject> GetRegisteredPlayers()
    {
        return _players;
    }
    public void RegisterEnemy(GameObject enemy)
    {
        Enemies_GameObjects.Register(enemy);
    }
    public void RegisterPlayer(GameObject player)
    {
        _players.Register(player);
    }
    public void RegisterRoom(GameObject room)
    {
        _rooms.Register(room);
    }
    public void RemovePlayer(GameObject player)
    {
        _players.Unregister(player);
    }
    [Button]
    public void ResetFloor()
    {
        _rooms.ForEach(r => r.GetComponent<RoomManager>().CleanUpRoom());
        _rooms.ForEach(r => r.GetComponent<RoomManager>().ResetRoom());
        activeRoom.EnterRoom();
    }
    public void Respawn()
    {
        activeRoom = staringoom;
        _players[0].transform.position = Respawnpoint;
        Camera.main.gameObject.GetComponent<CameraMovement>().maxPosition = new Vector2(
            GetRegisteredPlayers().First().transform.position.x,
            GetRegisteredPlayers().First().transform.position.y - 2);
        Enemies_GameObjects.Clear();
        Camera.main.gameObject.GetComponent<CameraMovement>().minPosition =
            Camera.main.gameObject.GetComponent<CameraMovement>().maxPosition;

        ResetFloor();
    }
    public void UnregisterEnemy(GameObject enemy)
    {
        Enemies_GameObjects.Unregister(enemy);
    }
    public void UnregisterRoom(GameObject room)
    {
        _rooms.Unregister(room);
    }
#endregion


#region Private Methods
    private void Start()
    {
        Respawnpoint = new Vector3(staringoom.transform.position.x + 14, staringoom.transform.position.y - 6, 0);
        Instantiate(
            player,
            Respawnpoint,
            Quaternion.identity);

        Instantiate(
            camera,
            new Vector3(
                GetRegisteredPlayers().First().transform.position.x,
                GetRegisteredPlayers().First().transform.position.y - 2,
                -10),
            Quaternion.identity);

        uiCanvas = Instantiate(pfbUICanvas);
    }
#endregion
}
public enum GameState
{
    Playing,
    Pause
}