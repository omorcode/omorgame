﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;


public class SoundManager : MonoBehaviour
{
    public float SfxVolume = 1f;
    public float MusicVolume = 1f;

    [SerializeField] protected AudioSource BackroundSource;
    protected List<AudioSource> audioSources = new List<AudioSource>();

    public float scrapTime = 1f;
    private float _scrapTime = 0f;

    public static SoundManager Main;

    private void Awake()
    {
        if (Main == null)
        {
            Main = this;
        }
        else if (Main != this)
        {
            Destroy(this);
        }
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        BackroundSource.Play();
        BackroundSource.volume = MusicVolume;
    }
    /* TODO: 
     * SfxVolume & MusicVolume apply changes
     * function in pauseMenu as it should?
     * script for backroundmusic in different scenes
     * (ui manager combine with soundmanager)
     * dont make the soundmanger addicted to anything
     * dicionary in production
     */

    public void PlayNewSound(AudioClip clip,float volumeMultiplier = 1f)
    {
        AudioSource source = gameObject.AddComponent<AudioSource>();
        SetupSource(source, clip, volumeMultiplier);
        audioSources.Add(source);
    }

    public void PlayNewSound(AudioClip clip, GameObject parent, float volumeMultiplier = 1f)
    {
        AudioSource source = parent.AddComponent<AudioSource>();
        SetupSource(source, clip, volumeMultiplier);
        audioSources.Add(source);
    }
    public void PlayNewSound(AudioClip clip, Vector3 position, float volumeMultiplier = 1f)
    {
        GameObject empty = Instantiate(new GameObject(), position, Quaternion.identity);
        AudioSource source = empty.AddComponent<AudioSource>();
        empty.AddComponent<SuicideBomber>();
        SetupSource(source, clip, volumeMultiplier);
    }

    private void SetupSource(AudioSource source,AudioClip clip, float volumeMultiplier)
    {
        source.clip = clip;
        source.volume = SfxVolume * volumeMultiplier;
        source.Play();
    }

    private void Update()
    {
        _scrapTime += Time.deltaTime;
        if (_scrapTime >= scrapTime)
        {
            Scrap();
            _scrapTime = 0f;
        }
    }

    private void Scrap()
    {
        List<AudioSource> toDelete = new List<AudioSource>();
        foreach (AudioSource source in audioSources)
        {
            if (!source.isPlaying)
            {
                toDelete.Add(source);
            }
        }
        foreach (AudioSource source in toDelete)
        {
            audioSources.Remove(source);
            Destroy(source);
        }
    }
}
