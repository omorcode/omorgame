using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;


public class RoomManager : MonoBehaviour
{
#region Fields
    public List<EnemySpawner> activeSpawner = new List<EnemySpawner>();
    [ShowInInspector] public RoomState BeginningRoomState;
    [ShowInInspector] public RoomState CurrentRoomState;
    public List<GameObject> enemySpawners = new List<GameObject>();
    public bool isStartroom;
    public double normalMoneyDropChance = 0.2;
    public UnityEvent RoomClearedExtras;
    public double rushHourMoneyDropChance = 0.4;
    public UnityEvent RushHourRoomClearedExtras;
    public List<GameObject> rushHourSpawners = new List<GameObject>();
    private readonly List<GameObject> _corpses = new List<GameObject>();
    private readonly List<GameObject> _roomTransitions = new List<GameObject>();
#endregion


#region Properies
    public bool AllowNextWave { get; private set; }
    [ShowInInspector] public RoomSpawnerstate CurrentSpawnerState { get; private set; }
#endregion


#region Public Methods
    [Button]
    public void AllSpawnerFinisheCheck()
    {
        if (activeSpawner.All(a => a.SpawnerIsFished)) ChangeSpawnerStatus(RoomSpawnerstate.cleared);
    }
    public void ChangeSpawnerStatus(RoomSpawnerstate newState)
    {
        switch (newState)
        {
            case RoomSpawnerstate.inactive:
                break;
            case RoomSpawnerstate.active:
                StartTheSpawning();

                break;
            case RoomSpawnerstate.cleared:
                switch (CurrentRoomState)
                {
                    case RoomState.friendly:
                        break;
                    case RoomState.unfriendly:
                        RoomClearedExtras?.Invoke();

                        break;
                    case RoomState.rushhour:
                        RushHourRoomClearedExtras?.Invoke();

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                ActivateTransitions();

                break;
            case RoomSpawnerstate.setup:
                SetUpRoom();

                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
        }

        CurrentSpawnerState = newState;
    }
    public void ChangStatus(RoomState newState)
    {
        switch (newState)
        {
            case RoomState.friendly:
                break;
            case RoomState.unfriendly:
                GetSpawner(enemySpawners);

                break;
            case RoomState.rushhour:
                GetSpawner(rushHourSpawners);

                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
        }

        CurrentRoomState = newState;
    }
    [Button]
    public void CleanUpRoom()
    {
        activeSpawner.ForEach(a => a.ResetSpawner());
        _corpses.ForEach(c => Destroy(c));
        _corpses.Clear();
    }
    public void EnterRoom()
    {
        GameManager.Main.activeRoom = this;
        switch (CurrentRoomState)
        {
            case RoomState.friendly:
                break;
            default:
                if (CurrentSpawnerState == RoomSpawnerstate.inactive) ChangeSpawnerStatus(RoomSpawnerstate.setup);

                break;
        }
    }
    public void NewWaveCheck()
    {
        AllowNextWave =
            enemySpawners.All(
                s => s.GetComponent<EnemySpawner>().currentState == SpawnerState.WaveFinished ||
                     s.GetComponent<EnemySpawner>().SpawnerIsFished);
    }
    public void RegisterCorpse(GameObject corpse)
    {
        corpse.GetComponent<Corpse>().Room = this;
        _corpses.Register(corpse);
    }
    public void RegisterEnemySpawner(GameObject enemySpawner)
    {
        if (enemySpawner.GetComponent<EnemySpawner>().isRushHourSpawner)
        {
            rushHourSpawners.Register(enemySpawner);

            return;
        }

        enemySpawners.Register(enemySpawner);
    }
    public void RegisterTransitions(GameObject transition)
    {
        _roomTransitions.Register(transition);
    }
    public void ResetRoom()
    {
        ChangeSpawnerStatus(RoomSpawnerstate.inactive);
    }
    public void SpawnObject(GameObject spawnObject, GameObject spawnpoint)
    {
        Instantiate(
            spawnObject,
            spawnpoint.transform.position,
            Quaternion.identity);
    }
    public void SpawnObjectMiddleOfRoom(GameObject spawnObject)
    {
        Instantiate(
            spawnObject,
            new Vector3(transform.position.x + 14, transform.position.y - 8, 0),
            Quaternion.identity,
            transform);
    }
    public void UnregisterCorpse(GameObject corpse)
    {
        _corpses.Unregister(corpse);
    }
#endregion


#region Private Methods
    private void ActivateTransitions()
    {
        _roomTransitions.ForEach(r => r.SetActive(true));
    }
    private void Awake()
    {
        GameManager.Main.RegisterRoom(gameObject);
        CurrentSpawnerState = RoomSpawnerstate.inactive;
    }
    private void DeactivateTransitions()
    {
        _roomTransitions.ForEach(r => r.SetActive(false));
    }
    private void GetSpawner(List<GameObject> spawnerList)
    {
        activeSpawner.Clear();
        spawnerList.ForEach(e => activeSpawner.Add(e.GetComponent<EnemySpawner>()));
    }
    private void OnEnable()
    {
        if (isStartroom) GameManager.Main.ChangStartingroom(this);
    }
    private void SetUpRoom()
    {
        CleanUpRoom();
        DeactivateTransitions();
        StartCoroutine(Wait(0.5f, RoomSpawnerstate.active));
    }
    private void Start()
    {
        ChangStatus(BeginningRoomState);
        if (GameManager.Main.staringoom == this) EnterRoom();
    }
    [Button]
    private void StartTheSpawning()
    {
        activeSpawner.ForEach(a => a.StartTheSpawning());
    }
    private IEnumerator Wait(float waitTimeInSeconds, RoomSpawnerstate stateAfterWait)
    {
        yield return new WaitForSeconds(waitTimeInSeconds);
        ChangeSpawnerStatus(stateAfterWait);
    }
#endregion


    /*TODO
     * room transitions registrieren und deaktivieren
     */
}
public enum RoomState
{
    friendly,
    unfriendly,
    rushhour
}
public enum RoomSpawnerstate
{
    inactive,
    setup,
    active,
    cleared
}