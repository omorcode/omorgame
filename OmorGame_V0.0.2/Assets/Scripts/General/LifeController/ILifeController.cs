﻿public enum Element
{
    Normal = 0,
    Piercing = 1,
    Explosion = 2,
    Slime = 3,
    Fire = 4,
    Water = 5,
    Ice = 6,
    Lightning = 7,
    Knockback =8
}

public interface ILifeController
{

    #region Properties
    bool Damageable { get; set; }
    float Lifepoints { get; set; }
    float MaxLifepoints { get; set; }
    #endregion

    #region Methods
    void DoDmg(float amount, Element element = Element.Normal);
    void Heal(float amount);
    #endregion

}