﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;


[Serializable]
public class PlayerLifeController : MonoBehaviour, ILifeController
{
#region Fields
    public bool DebugUnkillablePlayer;
    [ShowInInspector] public float lifepoints;
    public Player player;
    public UnityEvent playerDeath;
    public PlayerInput PlayerInput;
    private List<float> _lifechangers;
    [ShowInInspector] private float _maxLifepoints;
    private bool isImune;
#endregion


#region Public Methods
    public void Respawn()
    {
        StartCoroutine(Respawntimer(GameManager.Main.Respawnpoint));
    }
#endregion


#region Private Methods
    private void Awake()
    {
        isImune = false;
        MaxLifepoints = gameObject.GetComponent<PlayerAttributes>().LifeTotal;
        Lifepoints = MaxLifepoints;
    }
    private IEnumerator imunity()
    {
        isImune = true;

        yield return new WaitForSeconds(.5f);
        isImune = false;
    }
    private IEnumerator Respawntimer(Vector3 Spawnpoint)
    {
        PlayerInput.enabled = false;
        MaxLifepoints = gameObject.GetComponent<PlayerAttributes>().LifeTotal;
        Lifepoints = MaxLifepoints;
        GameManager.Main.Respawn();

        yield return new WaitForSeconds(0.1f);
        PlayerInput.enabled = true;
    }
#endregion


    public bool Damageable { get; set; }
    [ShowInInspector]
    public float Lifepoints
    {
        get => lifepoints;
        set
        {
            lifepoints = value;

            if (DebugUnkillablePlayer) return;
            if (value <= 0) playerDeath.Invoke();
        }
    }
    public float MaxLifepoints
    {
        get => _maxLifepoints;
        set => _maxLifepoints = value;
    }
    public void DoDmg(float amount, Element element = Element.Normal)
    {
        if (isImune) return;
        Lifepoints -= amount;
        StartCoroutine(imunity());
    }
    public void Heal(float amount)
    {
        Lifepoints += amount;
    }
}