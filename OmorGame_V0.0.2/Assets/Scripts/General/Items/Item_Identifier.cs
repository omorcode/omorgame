﻿using Sirenix.OdinInspector;
using UnityEngine;

public class Item_Identifier : MonoBehaviour
{

    #region Fields
    private Collider2D _itemCollider;
    [SerializeField] [ShowInInspector] private Item_SOS _item;
    #endregion

    #region Properties
    public Item_SOS Item { get; private set; }
    #endregion

    #region Methods
    private void Awake()
    {
        _itemCollider = GetComponent<Collider2D>();
        Item = _item;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Player>())
        {
            other.GetComponent<Inventory>().AddItem(Item);
            Destroy(gameObject);
        }
    }
    #endregion

}