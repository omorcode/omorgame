﻿using UnityEngine;

[CreateAssetMenu(fileName = "Recipe_", menuName = "Item/Recipe", order = 3)]
public class Recipe_SOS : Item_SOS
{
    #region Public Fields + Properties

    public Collectable_SOS[] ingriedients;

    #endregion Public Fields + Properties
}