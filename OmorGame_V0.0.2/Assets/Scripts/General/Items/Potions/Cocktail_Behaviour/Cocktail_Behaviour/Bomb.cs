﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;


public sealed class Bomb : AB_Bullet
{
#region Fields
    public List<GameObject> damageReciever;
    public bool deadenemyMode;
    public float explosionsRadius;
    public float timeUntilExplosion;
    private Collider2D _c2D;
    private float _t1;
    private bool _exploded;
#endregion


#region Public Methods
    public void Explode()
    {
        FindEnemiesAndDoDMG();
        _exploded = true;
    }
    [Button]
    public void FindEnemiesAndDoDMG()
    {
        damageReciever.ForEach(d => player.DoExplosionDamage(d, dmg));
        Destroy(gameObject);
    }
    public override void WriteValues()
    {
        _c2D = gameObject.GetComponent<Collider2D>();
        bulletLifeTime = attackTime;
        _t1 = timeUntilExplosion;
        base.WriteValues();
        _exploded = false;
    }
#endregion


#region Protected Methods
    protected override void FixedUpdateOperations()
    {
        DebugDrawer();
        _t1 = _t1 - Time.fixedDeltaTime;
        if (_t1 <= 0 && _exploded == false) Explode();

        if (!isFlying) return;
        bulletLifeTime = bulletLifeTime - Time.fixedDeltaTime;
        bulletRigidbody2D.MovePosition(bulletRigidbody2D.position + direction * speed * Time.fixedDeltaTime);
        if (bulletLifeTime <= 0) isFlying = false;
    }
#endregion


#region Private Methods
    private void Awake()
    {
        damageReciever = new List<GameObject>();
    }
    private void DebugDrawer()
    {
        CastRayStar(transform.position, explosionsRadius);
    }
    private void OnBecameInvisible()
    {
        //Debug.Log("i need to overie this");
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        damageReciever.Add(collision.gameObject);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        damageReciever.Remove(collision.gameObject);
    }
#endregion
}