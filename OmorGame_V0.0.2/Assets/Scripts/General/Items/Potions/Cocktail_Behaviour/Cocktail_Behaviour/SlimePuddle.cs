﻿using System.Collections;
using UnityEngine;


public class SlimePuddle : AttackBehaviour
{
#region Fields
    public float survivalTime;
#endregion


#region Private Methods
    private void Awake()
    {
        StartCoroutine(DestoyAfterTime());
    }
    private IEnumerator DestoyAfterTime()
    {
        yield return new WaitForSeconds(survivalTime);
        Destroy(gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Enemy>() is Enemy enemy) enemy.enemyMovement.SpeedMultiplier /= 2;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Enemy>() is Enemy enemy) enemy.enemyMovement.SpeedMultiplier *= 2;
    }
#endregion
}