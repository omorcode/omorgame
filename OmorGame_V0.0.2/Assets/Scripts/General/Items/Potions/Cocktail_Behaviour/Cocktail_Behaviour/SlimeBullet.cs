﻿using System.Linq;
using UnityEngine;


public class SlimeBullet : AB_Bullet
{
#region Fields
    public float explosionsRadius;
    public GameObject prefabSlimePuddle;
    public float timeUntilExplosion;
    private Collider2D _c2D;
    private GameObject _slimespot;
    private float _tte;
    private bool _exploded;
    private Player _player;
#endregion


#region Public Methods
    /// <summary>
    ///     instantiate a new slimespot
    /// </summary>
    public void CreateSlimespot()
    {
        _slimespot = Instantiate(prefabSlimePuddle, transform.position, transform.rotation);
    }
    /// <summary>
    ///     The bomp Explodes and doas dmg.
    /// </summary>
    public void Explode()
    {
        CreateSlimespot();
        Destroy(gameObject);
        _exploded = true;
    }
    public override void WriteValues()
    {
        _player = GameManager.Main.GetRegisteredPlayers().First().GetComponent<Player>();
        _c2D = gameObject.GetComponent<Collider2D>();
        bulletLifeTime = attackTime;
        _tte = timeUntilExplosion;
        base.WriteValues();
        _exploded = false;
    }
#endregion


#region Protected Methods
    protected override void FixedUpdateOperations()
    {
        DebugDrawer();
        _tte = _tte - Time.fixedDeltaTime;
        if (_tte <= 0 && _exploded == false) Explode();

        if (!isFlying) return;
        bulletLifeTime = bulletLifeTime - Time.fixedDeltaTime;
        bulletRigidbody2D.MovePosition(bulletRigidbody2D.position + direction * speed * Time.fixedDeltaTime);
        if (bulletLifeTime <= 0)
        {
            isFlying = false;
            Explode();
        }
    }
#endregion


#region Private Methods
    private void DebugDrawer()
    {
        Debug.DrawRay(transform.position, Vector2.up * explosionsRadius, Color.red);
        Debug.DrawRay(transform.position, Vector2.down * explosionsRadius, Color.red);
        Debug.DrawRay(transform.position, Vector2.right * explosionsRadius, Color.red);
        Debug.DrawRay(transform.position, Vector2.left * explosionsRadius, Color.red);
        Debug.DrawRay(transform.position, new Vector2(1, 1).normalized * explosionsRadius, Color.red);
        Debug.DrawRay(transform.position, new Vector2(1, -1).normalized * explosionsRadius, Color.red);
        Debug.DrawRay(transform.position, new Vector2(-1, 1).normalized * explosionsRadius, Color.red);
        Debug.DrawRay(transform.position, new Vector2(-1, -1).normalized * explosionsRadius, Color.red);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_player.DoSlimeDamage(collision.gameObject, dmg)) Destroy(gameObject);
    }
#endregion


    /*
     * TODO
     * - es nimmt sich den player aus dem Gammanager
     * - für #Multiplayer fixen
     */
}