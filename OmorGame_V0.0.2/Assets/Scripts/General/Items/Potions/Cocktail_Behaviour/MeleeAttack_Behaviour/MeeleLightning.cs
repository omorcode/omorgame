public class MeeleLightning : AB_MeeleCharged
{
#region Protected Methods
    protected override void Action()
    {
        DMGtoAll(dmg, Element.Lightning);
        DMGtoAll(dmg);
    }
#endregion
}