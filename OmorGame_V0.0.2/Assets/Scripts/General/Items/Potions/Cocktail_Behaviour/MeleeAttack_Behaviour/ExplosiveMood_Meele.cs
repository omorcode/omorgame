using UnityEngine;


public class ExplosiveMood_Meele : AB_Meele
{
#region Fields
    public GameObject PFBBomb;
    private LayerMask _layermask;
#endregion


#region Protected Methods
    protected override void Action()
    {
        DMGtoAll(dmg, Element.Explosion);
        DeadEnemyBomb();
    }
#endregion


#region Private Methods
    private void Awake()
    {
        _layermask = LayerMask.GetMask(LayerMask.LayerToName(16));
    }
    private void DeadEnemyBomb()
    {
        var direction = player.PlayerMoveControllerCO.currentCardinal;
        var hit2D = Physics2D.Raycast(transform.position, direction, 5f, _layermask);
        Debug.DrawRay(transform.position, direction * 5, Color.cyan, 1f);
        if (hit2D)
            if (hit2D.transform.gameObject.GetComponent<StunnAndKnockback>() is StunnAndKnockback enemyStunnAndKnockback
            )
            {
                var clone = Instantiate(PFBBomb, hit2D.transform);
                clone.transform.localPosition = Vector3.zero;
                clone.GetComponent<BombOnEnmy>().player = player;
                hit2D.rigidbody.constraints = RigidbodyConstraints2D.None;
                hit2D.rigidbody.AddTorque(10);
                enemyStunnAndKnockback.Yeet(3f, 5f);
            }
    }
#endregion
}