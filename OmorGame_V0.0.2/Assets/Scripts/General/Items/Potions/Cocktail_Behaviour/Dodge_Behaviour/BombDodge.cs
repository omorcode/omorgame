﻿using UnityEngine;

public sealed class BombDodge : AB_Dodge
{
    #region Public Fields + Properties

    public GameObject prefabBomb;

    #endregion Public Fields + Properties

    #region Private Fields + Properties

    private GameObject _clone;
    private bool _didDodge = false;

    #endregion Private Fields + Properties

    #region Protected Methods

    protected override void Action()
    {
        base.Action();
        _didDodge = true;
    }

    protected override void FixedUpdateOperations()
    {
        if (_didDodge)
        {
            _clone = Instantiate(prefabBomb, gameObject.transform.position, gameObject.transform.rotation);
            _clone.GetComponent<AB_Bullet>().WriteValues();
            _clone.GetComponent<AB_Bullet>().dmg = dmg;
            _clone.GetComponent<AB_Bullet>().player = this.player;
            _didDodge = false;
        }
        base.FixedUpdateOperations();
    }

    #endregion Protected Methods
}