using System;
using System.Collections;
using UnityEngine;


public class StickyWhack_Dodge : AB_Dodge
{
#region Fields
    public GameObject prefabSlimePuddle;
    public float pudletime = 0.2f;
#endregion


#region Protected Methods
    protected override void Action()
    {
        base.Action();
        StartCoroutine(LayPuddles());
    }
#endregion


#region Private Methods
    private IEnumerator LayPuddles()
    {
        var runs = (int) Math.Truncate(attackTime / pudletime);
        for (var i = 0; i < runs; i++)
        {
            Instantiate(prefabSlimePuddle, transform.position, Quaternion.identity);

            yield return new WaitForSeconds(pudletime);
        }
    }
#endregion
}