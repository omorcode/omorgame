﻿using System.Collections.Generic;
using UnityEngine;

public sealed class SpearThrow_Directional : AB_Directional
{
    #region Public Fields + Properties

    public bool CanStab
    {
        private set;
        get;
    }

    public List<Element> sortedElementList = new List<Element> {
    Element.Normal
    };

    #endregion Public Fields + Properties

    #region Private Fields + Properties

    private int _elementIndex = 0;

    #endregion Private Fields + Properties

    #region Protected Methods

    protected override void Action()
    {
        BulletInstantiation();
        clone.GetComponent<SpearAura>().element = sortedElementList[_elementIndex];
        _elementIndex = (++_elementIndex == sortedElementList.Count - 1 ? 0 : _elementIndex);
        clone.GetComponent<Rigidbody2D>().RotationToNESW(direction);
        direction.LimitDirectionToNWES();

        gameObject.GetComponentInChildren<SpearStab>().refillTimer();

        LetFly();
    }

    protected override void FixedUpdateOperations()
    {
        base.FixedUpdateOperations();
    }

    #endregion Protected Methods
}