using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;


public class Lightning : AB_Bullet
{
#region Fields
    public int maxZapNumber;
    public float zaprange;
    private _bulletState _currentBulletState;
    private Action _updateAction;
    private List<GameObject> _enemies = new List<GameObject>();
    private int _zapcounter;
    public bool scootRandom;
#endregion


#region Public Methods
    public override void WriteValues()
    {
        base.WriteValues();
        _zapcounter = 0;
        ChangeStatus(_bulletState.flying);
        _enemies = new List<GameObject>();
        player = GameManager.Main.GetRegisteredPlayers().First().GetComponent<Player>();
    }
#endregion


#region Protected Methods
    protected override void FixedUpdateOperations()
    {
        _updateAction?.Invoke();
    }
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (player.DoLightningDamage(collision.gameObject, dmg) && !_enemies.Contains(collision.gameObject))
        {
            _enemies.Add(collision.gameObject);
            ChangeStatus(_bulletState.zapping);
            return;
        }


        ChangeStatus((scootRandom)? _bulletState.randomAway: _bulletState.destroying);
    }
#endregion


#region Private Methods
    private void ChangeStatus(_bulletState newState)
    {
        switch (newState)
        {
            case _bulletState.flying:
                _updateAction = Flying;

                break;
            case _bulletState.zapping:
                _updateAction = null;
                Zapping();

                break;
            case _bulletState.destroying:
                _updateAction = DestroyBullet;

                break;
            case _bulletState.randomAway:
                _updateAction = null;
                goesScoot();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
        }
    }


    private void DestroyBullet()
    {
        Destroy(gameObject);
    }
    private void Flying()
    {
        bulletRigidbody2D.MovePosition(bulletRigidbody2D.position + direction * speed * Time.fixedDeltaTime);
    }
    private void Zapping()
    {
        var closeEnemies = GameManager.Main.Enemies_GameObjects
            .Where(
                e => Vector3.Distance(e.transform.position, transform.position) < zaprange &&
                     !_enemies.Contains(e.gameObject))
            .OrderByDescending(e => Vector3.Distance(e.transform.position, transform.position))
            .ToList();

        if (closeEnemies.Any() && _zapcounter < maxZapNumber )
        {
            direction = (closeEnemies.First().transform.position - transform.position).normalized;
            ChangeStatus(_bulletState.flying);
            _zapcounter++;

            return;
        }

        ChangeStatus(_bulletState.destroying);
    }
    private Unity.Mathematics.Random r = new Unity.Mathematics.Random(42);
    private void goesScoot()
    {
        Debug.LogWarning("Scooooot");
        Vector3 randomDir = new Vector2(r.NextFloat(1), r.NextFloat(1));
        direction = (randomDir - transform.position).normalized;
        ChangeStatus(_bulletState.flying);


    }

#endregion


    /*TODO
     * - zaprange & zap counter in Gamedsigner Script
     *
     */
    private enum _bulletState
    {
        flying,
        zapping,
        destroying,
        randomAway,
    }
}