using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;


public class InventoryUI : SerializedMonoBehaviour
{
#region Fields
    public GameObject firstInventorySlot;
    [TableMatrix(HorizontalTitle = "X axis", VerticalTitle = "Y axis")] [OdinSerialize]
    public Image[,] images = new Image[2, 4];
    public Inventory playerInventory;
    [OdinSerialize] public Dictionary<Button, Potion_SOS> potioncounter = new Dictionary<Button, Potion_SOS>();
    private int2 _selectedItem;
    private Color save;
    private EventSystem test;
    private InputSystemUIInputModule test1;
#endregion


#region Public Methods
    public void UpdateText()
    {
        foreach (var BPP in potioncounter)
            if (playerInventory.PlayerInventory.Where(p => p.item == BPP.Value).Any())
            {
                BPP.Key.GetComponentInParent<InventorySlotUI>().InventorySlot =
                    playerInventory.PlayerInventory.Where(p => p.item == BPP.Value).First();

                BPP.Key.GetComponentInParent<InventorySlotUI>().updateThis.Invoke();
            }

        //BPP.Key.GetComponentInChildren<TextMesh>().text =
    }
#endregion


#region Private Methods
    private void OnBecameVisible()
    {
        test.firstSelectedGameObject = firstInventorySlot;
        EventSystem.current.SetSelectedGameObject(firstInventorySlot, null);
    }
    private void Start()
    {
        test = FindObjectOfType<EventSystem>();
        playerInventory = GameManager.Main.GetRegisteredPlayers()[0].GetComponent<Inventory>();
        test1 = FindObjectOfType<InputSystemUIInputModule>();
        playerInventory.InventoryChanged.AddListener(UpdateText);
    }
#endregion
}