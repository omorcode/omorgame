﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;


public class HeartsUI : MonoBehaviour
{
#region Fields
    public int currentHealth;
    public Sprite emptyHeart;
    public Sprite fullHeart;
    public Image[] hearts;
    public int maxHealth;
    private GameObject _player;
    private PlayerLifeController _playerLifeController;
#endregion


#region Public Methods
    public void OnEscape(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            if (GameManager.Main.GameState == GameState.Pause)
                GameManager.Main.ChangeGameState(GameState.Playing);
            else if (GameManager.Main.GameState == GameState.Playing) GameManager.Main.ChangeGameState(GameState.Pause);
        }
    }
#endregion


#region Private Methods
    private void Start()
    {
        _player = GameManager.Main.GetRegisteredPlayers()[0];
        _playerLifeController = _player.GetComponent<PlayerLifeController>();
    }
    private void Update()
    {
        currentHealth = Mathf.RoundToInt(_playerLifeController.Lifepoints);
        maxHealth = Mathf.RoundToInt(_playerLifeController.MaxLifepoints);
        for (var i = 0; i < hearts.Length; i++)
        {
            if (i < currentHealth)
                hearts[i].sprite = fullHeart;
            else
                hearts[i].sprite = emptyHeart;

            if (i < maxHealth)
                hearts[i].enabled = true;
            else
                hearts[i].enabled = false;
        }
    }
#endregion
}