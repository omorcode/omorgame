using TMPro;
using UnityEngine;


public class CoinUI : MonoBehaviour
{
#region Fields
    public TextMeshProUGUI text;
    private GameObject _player;
    private Inventory _playerInventory;
#endregion


#region Private Methods
    // Start is called before the first frame update
    private void Start()
    {
        _player = GameManager.Main.GetRegisteredPlayers()[0];
        _playerInventory = _player.GetComponent<Inventory>();
        _playerInventory.addCoins.AddListener(UpdateCoins);
    }
    private void UpdateCoins()
    {
        text.text = _playerInventory.Coins.ToString();
    }
#endregion
}