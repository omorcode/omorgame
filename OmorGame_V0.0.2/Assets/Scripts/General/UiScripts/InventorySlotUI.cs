using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;


public class InventorySlotUI : MonoBehaviour
{
#region Fields
    public Inventory Inventory;
    public InventorySlot InventorySlot;
    public TextMeshProUGUI textCounter;
    public TextMeshProUGUI textName;
    public UnityEvent updateThis;
#endregion


#region Public Methods
    public void Use()
    {
        if (InventorySlot != null) Inventory.Use(InventorySlot);
    }
#endregion


#region Private Methods
    private void Start()
    {
        updateThis.AddListener(UpdateText);
        Inventory = GameManager.Main.GetRegisteredPlayers().First().GetComponent<Inventory>();
    }
    private void UpdateText()
    {
        if (InventorySlot == null)
        {
            textCounter.text = "0";
            textName.text = "Omor";

            return;
        }

        textCounter.text = InventorySlot.amount.ToString();
        textName.text = InventorySlot.item.name;
    }
#endregion
}