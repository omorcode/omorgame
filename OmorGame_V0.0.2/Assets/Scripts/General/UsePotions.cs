using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;


public class UsePotions : MonoBehaviour
{
    public Player player;
    public GameObject DrinkUp;
    public GameObject DrinkDown;
    public GameObject DrinkLeft;
    public GameObject DrinkRight;
    public int maxWainttime;
    public int _wainttime;
    public UnityEvent timerChanged;
    public int Wainttime
    {
        get => _wainttime;
        set
        {
            _wainttime = value;
            timerChanged.Invoke();
        }
    }
    public bool canDrink = true;

    public void ActivateDrink(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (!canDrink) return;
            switch (context.ReadValue<Vector2>())
            {
                case Vector2 v when v.Equals(Vector2.up) && FourDrinks.Main.HasFoundDrink(Drinks.flashMob) :
                    player.PlayerAttackCO.LoadWeapon(DrinkUp);
                    StartCoroutine(wait());
                    break;
                case Vector2 v when v.Equals(Vector2.down) && FourDrinks.Main.HasFoundDrink(Drinks.beer):
                    player.PlayerAttackCO.LoadWeapon(DrinkDown);
                    StartCoroutine(wait());

                    break;
                case Vector2 v when v.Equals(Vector2.left) && FourDrinks.Main.HasFoundDrink(Drinks.sticky):
                    player.PlayerAttackCO.LoadWeapon(DrinkLeft);
                    StartCoroutine(wait());

                    break;
                case Vector2 v when v.Equals(Vector2.right) && FourDrinks.Main.HasFoundDrink(Drinks.explode):
                    player.PlayerAttackCO.LoadWeapon(DrinkRight); 
                    StartCoroutine(wait());
                    break;
            }

           
        }
    }

    private IEnumerator wait()
    {
        canDrink = false;
        Wainttime = maxWainttime;
        while (Wainttime >0)
        {
            yield return new WaitForSeconds(.8f);
            Wainttime--;
        }

        canDrink = true;

    }



}