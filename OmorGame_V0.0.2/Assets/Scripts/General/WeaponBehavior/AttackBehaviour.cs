﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;


public abstract class AttackBehaviour : MonoBehaviour
{
#region Fields
    [HideIf(nameof(hideFromGamedesigners))]
    [TabGroup("Speed Calculation")]
    [PropertyRange(1f, 25f)]
    [OnValueChanged(nameof(CalculateSpeed))]
    [LabelText("$attackRangeName")]
    public float attackrange = 1f;
    [HideIf(nameof(hideFromGamedesigners))]
    [TabGroup("Speed Calculation")]
    [PropertyRange(0.00001f, 15f)]
    [LabelText("$attackTimeName")]
    [OnValueChanged(nameof(CalculateSpeed))]
    public float attackTime = 1f;
    [HideIf(nameof(hideFromGamedesigners))]
    [TabGroup("Attack Values")]
    [PropertyOrder(1f)]
    [PropertyRange(0f, 5f)]
    [LabelText("$chargeTimeName")]
    public float chargetime;
    [HideIf(nameof(hideFromGamedesigners))]
    [TabGroup("Attack Values")]
    [PropertyOrder(1.2f)]
    [PropertyRange(0f, 5f)]
    [LabelText("$cooldownName")]
    public float cooldown;
    public string displayName;
    [TabGroup("Attack Values")]
    [HideIf(nameof(hideFromGamedesigners))]
    [PropertyOrder(1.1f)]
    [PropertyRange(0, 50)]
    [LabelText("$dmgName")]
    public float dmg;
    public bool hideFromGamedesigners;
    [ReadOnly] public List<GameObject> opponents;
    public Player player;
    [HideIf(nameof(hideFromGamedesigners))] [TabGroup("Speed Calculation")] [LabelText("$speedName")]
    public float speed;
    public bool stopsMovement;
    protected string chargeTimeName = "Charge Time";
    protected string dmgName = "DMG";
    protected string cooldownName = "Cooldown";
    protected string speedName = "Speed";
    protected string attackRangeName = "Attack Range";
    protected string attackTimeName = "Attack Time ";
    [TabGroup("Attack Values")]
    [ShowInInspector]
    [PropertyOrder(1.21f)]
    [ReadOnly]
    [ProgressBar(0f, nameof(cooldown))]
    [HideLabel]
    [HideIf(nameof(hideFromGamedesigners))]
    private float _t;
    [HideIf(nameof(hideFromGamedesigners))] [TabGroup("Speed Calculation")] [InfoBox("_speedCalculationInfo")]
    private string _speedCalculationInfo = "Speed is used for : test";
    [HideIf(nameof(_secreteMode))] private bool _secreteMode = true;
    public string AnnimationTrigger;
    public bool isDodge;
#endregion


#region Public Methods
    [OnInspectorInit(nameof(RenameInInspector))]
    //public float t1;
    public void Attack()
    {

        if (_t <= 0)
        {
            _t = cooldown;
            if (isDodge)
            {
                Action();
                return;
            }

            StartCoroutine(PlayAnimationAndAttack());
        }
    }

    public IEnumerator PlayAnimationAndAttack()
    {
        player.PlayerMoveControllerCO.DisableMovement();
        player.animator.SetTrigger(AnnimationTrigger);

        if (cooldown < player.animator.GetCurrentAnimatorClipInfo(0).Length)
        {
            _t = player.animator.GetCurrentAnimatorClipInfo(0).Length;
        }
        yield return new WaitForSeconds(player.animator.GetCurrentAnimatorClipInfo(0).Length);
            speed = attackrange / attackTime;
            Action();
            player.PlayerMoveControllerCO.EnableMovement();
    }

#endregion


#region Protected Methods
    protected virtual void Action() { }
    protected void CastRayStar(Vector2 startposition, float length)
    {
        Debug.DrawRay(startposition, Vector2.up * length, Color.red);
        Debug.DrawRay(startposition, Vector2.down * length, Color.red);
        Debug.DrawRay(startposition, Vector2.right * length, Color.red);
        Debug.DrawRay(startposition, Vector2.left * length, Color.red);
        Debug.DrawRay(startposition, new Vector2(1, 1).normalized * length, Color.red);
        Debug.DrawRay(startposition, new Vector2(1, -1).normalized * length, Color.red);
        Debug.DrawRay(startposition, new Vector2(-1, 1).normalized * length, Color.red);
        Debug.DrawRay(startposition, new Vector2(-1, -1).normalized * length, Color.red);
    }
    protected virtual void FixedUpdateOperations() { }
    protected void FixedUpdateStandartOperation()
    {
        _t = Timecheck(_t);
    }
    protected virtual void RenameInInspector()
    {
        chargeTimeName = "Charge Time";
        dmgName = "DMG";
        cooldownName = "Cooldown";
        speedName = "Speed";
        attackRangeName = "Attack Range";
        attackTimeName = "Attack Time ";
    }
#endregion


#region Private Methods
    private void CalculateSpeed()
    {
        speed = attackrange / attackTime;
    }
    private void FixedUpdate()
    {
        FixedUpdateStandartOperation();
        FixedUpdateOperations();
    }
    //private static float RangeClamp(float value, GUIContent label)
    //{
    //    return EditorGUILayout.Slider(label, value, 0f, 10f);
    //}
    private static float Timecheck(float localtime)
    {
        if (localtime <= 0) return 0;
        localtime = localtime - Time.fixedDeltaTime;

        return localtime;
    }

#endregion


    //   [HideInInspector]
}