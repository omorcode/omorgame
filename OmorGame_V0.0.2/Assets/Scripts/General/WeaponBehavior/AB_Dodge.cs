﻿using System.Collections;
using UnityEngine;


public class AB_Dodge : AttackBehaviour
{
#region Fields
    public float endIndestructability;
    public float startIndestructability;
    public Vector3 test;

    //private Vector2 startposition;
    private bool _isDodging;
    private GameObject _player;
    private ILifeController _playerLifeController;
    private PlayerMoveController _playerMoveController;
    private Rigidbody2D _playerRigidbody2D;
    private LayerMask _layermask;
    private Vector2 _direction;
#endregion


#region Public Methods
    public IEnumerator Indestructable()
    {
        yield return new WaitForSeconds(startIndestructability);
        player.gameObject.layer = 24;

        yield return new WaitForSeconds(endIndestructability);
        player.gameObject.layer = 9;
    }
#endregion


#region Protected Methods
    protected override void Action()
    {
        _playerMoveController.moving = false;
        _direction = _playerMoveController.currentCardinal;
        _playerLifeController.Damageable = false;
        StartCoroutine(Dodging());
    }
    protected override void FixedUpdateOperations()
    {
        if (!_isDodging) return;
        _playerRigidbody2D.MovePosition(_playerRigidbody2D.position + _direction * speed * Time.fixedDeltaTime);
    }
#endregion


#region Private Methods
    private void Awake()
    {
        _layermask = LayerMask.GetMask(LayerMask.LayerToName(22));
        _player = FindObjectOfType<Player>().gameObject;
        _playerMoveController = _player.GetComponent<PlayerMoveController>();
        _playerRigidbody2D = _player.GetComponent<Rigidbody2D>();
        _playerLifeController = _player.GetComponent<ILifeController>();
        isDodge = true;
    }
    private IEnumerator Dodging()
    {
        test = transform.position;
        _isDodging = true;
        StartCoroutine(Indestructable());

        yield return new WaitForSeconds(attackTime);
        _isDodging = false;
        //var RaycastHit = Physics2D.Raycast(
        //    new Vector3(transform.position.x, transform.position.y, transform.position.z - 2),
        //    Vector3.forward * 4,
        //    Mathf.Infinity,
        //    _layermask);

        //if (RaycastHit.transform != null)
        if (gameObject.IsOverCanyon())
        {
            player.PlayerLifeControllerCO.DoDmg(1);

            yield return new WaitForSeconds(0.5f);
            player.PlayeRigidbody2D.MovePosition(test);
        }

        _playerMoveController.moving = true;
    }
#endregion
}