using UnityEngine;


[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class AB_Bullet : AttackBehaviour
{
#region Fields
    [Tooltip("after this time the bullet is destroy if is has not collided with something before")]
    public float bulletLifeTime;
    public Rigidbody2D bulletRigidbody2D;
    public Vector2 direction;
    public bool isFlying;
    public float waitUntilMove;
    private CircleCollider2D _bulletCollider2D;
#endregion


#region Public Methods
    public virtual void WriteValues()
    {
        bulletRigidbody2D = gameObject.GetComponent<Rigidbody2D>();
    }
#endregion


#region Protected Methods
    protected override void FixedUpdateOperations()
    {
        if (waitUntilMove > 0) waitUntilMove = waitUntilMove - Time.fixedDeltaTime;
        {
            bulletLifeTime = bulletLifeTime - Time.fixedDeltaTime;
            bulletRigidbody2D.MovePosition(bulletRigidbody2D.position + direction * speed * Time.fixedDeltaTime);
            if (bulletLifeTime <= 0)
            {
                isFlying = false;
                Destroy(gameObject);
            }
        }
    }
    protected virtual void Start()
    {
        WriteValues();
        bulletLifeTime = attackTime;
    }
    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent(typeof(ILifeController)))
            collision.gameObject.GetComponent<ILifeController>().DoDmg(dmg);

        Destroy(gameObject);
    }
#endregion
}