﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;


[HideMonoScript]
public class LifeDebugger : MonoBehaviour
{
    #region Fields
    public ILifeController lifeController;
    #endregion


    #region Public Methods
    public void DoDamage(InputAction.CallbackContext context)
    {
        if (context.performed) lifeController.DoDmg(1);
    }
    public void DoHeal(InputAction.CallbackContext context)
    {
        if (context.performed) lifeController.Heal(2);
    }
    #endregion
}