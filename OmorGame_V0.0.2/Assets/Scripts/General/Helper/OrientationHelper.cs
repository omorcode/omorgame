﻿using System.Linq;
using UnityEngine;

public static class OrientationHelper
{
    #region Public Fields + Properties

    public static Vector2[] directions = new Vector2[]
   {
       Vector2.up,
       Vector2.down,
       Vector2.right,
       Vector2.left,
   };

    #endregion Public Fields + Properties

    #region Public Methods

    public static Quaternion GiveOrientation(Vector2 input)
    {
        switch (directions.OrderBy(d => (input - d).magnitude).First())
        {
            case Vector2 d when d.Equals(directions[0]):
                return Quaternion.Euler(0f, 0f, 0f);

            case Vector2 d when d.Equals(directions[1]):

                return Quaternion.Euler(0f, 0f, 180f);

            case Vector2 d when d.Equals(directions[2]):

                return Quaternion.Euler(0f, 0f, 270f);

            case Vector2 d when d.Equals(directions[3]):
                return Quaternion.Euler(0f, 0f, 90f);

            default:
                return Quaternion.Euler(0f, 0f, 0f);
        }
    }

    public static Vector2 GiveDirection(this Transform t)
    {
        switch (t.rotation)
        {
            case Quaternion q when q.Equals(Quaternion.Euler(0f, 0f, 0)):
                return directions[0];

            case Quaternion q when q.Equals(Quaternion.Euler(0f, 0f, 180f)):
                return directions[1];

            case Quaternion q when q.Equals(Quaternion.Euler(0f, 0f, 270f)):
                return directions[2];

            case Quaternion q when q.Equals(Quaternion.Euler(0f, 0f, 90f)):
                return directions[3];

            default:
                Debug.LogError("default case in switchs" + t.rotation);
                return Vector2.zero;
        }
    }

    public static void RotationToNESW(this Rigidbody2D rb, Vector2 input)
    {
        rb.transform.rotation = GiveOrientation(input);
    }

    public static void RotationToNESW(this Transform t, Vector2 input)
    {
        t.rotation = GiveOrientation(input);
    }

    public static void AssignOrientation(this Transform t, Vector2 v)
    {
        if (v == Vector2.zero) return;
        t.rotation = GiveOrientation(v);
    }

    public static void LimitDirectionToNWES(this ref Vector2 direction)
    {
        var temp = direction;
        direction = directions.OrderBy(d => (temp - d).magnitude).First();
    }

    #endregion Public Methods
}