using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class CollectionHelperU 
{
    public static void Register(this List<GameObject> RegistrationList, GameObject objectToRegister)
    {
        if (RegistrationList.Any(e => e == objectToRegister)) return;
        RegistrationList.Add(objectToRegister);
    }

    public static void Unregister(this List<GameObject> RegistrationList, GameObject objectToUnregister)
    {
        if (RegistrationList.Any(p => p == objectToUnregister.gameObject))
        {
            RegistrationList.Remove(objectToUnregister.gameObject);
        }
    }
}
