﻿using System.Collections;
using UnityEngine;


public class CameraMovement : MonoBehaviour
{
#region Fields
    public Vector2 maxPosition;
    public Vector2 minPosition;
    public float smoothing;
    public Transform target;
#endregion


#region Private Methods
    private IEnumerator FindPlayer()
    {
        while (target == null)
        {
            if (GameManager.Main.GetRegisteredPlayers().Count == 1)
                target = GameManager.Main.GetRegisteredPlayers()[0].transform;

            maxPosition = new Vector2(target.position.x, target.position.y - 2);
            minPosition = new Vector2(target.position.x, target.position.y - 2);

            yield return new WaitForSeconds(0.1f);
        }
    }
    private void LateUpdate()
    {
        if (transform.position != target.position)
        {
            var targetPosition = new Vector3(target.position.x, target.position.y, transform.position.z);
            targetPosition.x = Mathf.Clamp(targetPosition.x, minPosition.x, maxPosition.x);
            targetPosition.y = Mathf.Clamp(targetPosition.y, minPosition.y, maxPosition.y);
            transform.position = Vector3.Lerp(transform.position, targetPosition, smoothing);
        }
    }
    private void Start()
    {
        StartCoroutine(FindPlayer());
        DontDestroyOnLoad(gameObject);
    }
#endregion
}