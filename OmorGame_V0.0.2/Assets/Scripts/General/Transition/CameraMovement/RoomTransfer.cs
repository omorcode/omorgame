﻿using UnityEngine;


public class RoomTransfer : MonoBehaviour
{
#region Fields
    public Vector2 cameraMaxChange;
    public Vector2 cameraMinChange;
    public RoomManager nextRoom;
    public Vector3 playerChange;
    private CameraMovement _cam;
#endregion


#region Public Methods
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (nextRoom == null)
        {
            Debug.LogError("There is no room to enter. NextRoom is null");

            return;
        }

        _cam.minPosition += cameraMinChange;
        _cam.maxPosition += cameraMaxChange;
        other.transform.position += playerChange;
        nextRoom.EnterRoom();
    }
#endregion


#region Private Methods
    private void Start()
    {
        _cam = Camera.main.GetComponent<CameraMovement>();
        gameObject.GetComponentInParent<RoomManager>().RegisterTransitions(gameObject);
    }
#endregion
}