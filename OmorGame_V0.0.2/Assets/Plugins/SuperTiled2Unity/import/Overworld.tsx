<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="Overworld" tilewidth="16" tileheight="16" tilecount="1440" columns="40">
 <image source="Overworld.png" width="640" height="576"/>
 <terraintypes>
  <terrain name="path" tile="-1"/>
  <terrain name="Water" tile="0"/>
  <terrain name="weg" tile="0"/>
  <terrain name="New Terrain" tile="0"/>
 </terraintypes>
 <tile id="242" terrain=",,,1"/>
 <tile id="243" terrain=",,1,1"/>
 <tile id="244" terrain=",,1,"/>
 <tile id="282" terrain=",1,,1"/>
 <tile id="283" terrain="1,1,1,1"/>
 <tile id="284" terrain="1,,1,"/>
 <tile id="322" terrain=",1,,"/>
 <tile id="323" terrain="1,1,,"/>
 <tile id="324" terrain="1,,,"/>
 <tile id="362" terrain="1,1,1,"/>
 <tile id="363" terrain="1,1,,1"/>
 <tile id="402" terrain="1,,1,1"/>
 <tile id="403" terrain=",1,1,1"/>
 <tile id="1011" terrain=",,,3"/>
 <tile id="1012" terrain=",,3,3"/>
 <tile id="1013" terrain=",,3,"/>
 <tile id="1014" terrain="3,3,3,"/>
 <tile id="1015" terrain="3,3,,3"/>
 <tile id="1051" terrain=",3,,3"/>
 <tile id="1052" terrain="3,3,3,3"/>
 <tile id="1053" terrain="3,,3,"/>
 <tile id="1054" terrain="3,,3,3"/>
 <tile id="1055" terrain=",3,3,3"/>
 <tile id="1091" terrain=",3,,"/>
 <tile id="1092" terrain="3,3,,"/>
 <tile id="1093" terrain="3,,,"/>
 <tile id="1160" terrain=",,,2"/>
 <tile id="1161" terrain=",,2,2"/>
 <tile id="1162" terrain=",,2,"/>
 <tile id="1200" terrain=",2,,2"/>
 <tile id="1201" terrain="2,2,2,2"/>
 <tile id="1202" terrain="2,,2,"/>
 <tile id="1240" terrain=",2,,"/>
 <tile id="1241" terrain="2,2,,"/>
 <tile id="1242" terrain="2,,,"/>
 <tile id="1280" terrain="2,2,2,"/>
 <tile id="1281" terrain="2,2,,2"/>
 <tile id="1320" terrain="2,,2,2"/>
 <tile id="1321" terrain=",2,2,2"/>
</tileset>
