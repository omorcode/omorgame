﻿using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;

#if UNITY_EDITOR

[InitializeOnLoad]
#endif
public class CardinalDirections8 : InputProcessor<Vector2>
{
    #region Constructor
#if UNITY_EDITOR
    
    static CardinalDirections8()
    {
        Initialize();
    }
#endif
#endregion


#region Public Methods
    public static Vector2 Cardinalize(Vector2 value)
    {
        return new Vector2(Mathf.Round(value.x), Mathf.Round(value.y));
    }
    public static Vector3 Cardinalize(Vector3 value)
    {
        return new Vector3(Mathf.Round(value.x), Mathf.Round(value.y), Mathf.Round(value.z));
    }
    public override Vector2 Process(Vector2 value, InputControl control)
    {
        return new Vector2(Mathf.Round(value.x), Mathf.Round(value.y));
    }
#endregion


#region Private Methods
    [RuntimeInitializeOnLoadMethod]
    private static void Initialize()
    {
        InputSystem.RegisterProcessor<CardinalDirections8>();
    }
#endregion
}